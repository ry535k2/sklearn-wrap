import unittest
import numpy as np
import pickle
from typing import NamedTuple

from sklearn import datasets
from sklearn.utils import shuffle

from . import SKBernoulliNB
from d3m import container
from pandas.util.testing import assert_frame_equal
from common_primitives import dataset_to_dataframe, column_parser
# Common random state
rng = np.random.RandomState(0)

# Load the iris dataset and randomly permute it
iris = datasets.load_iris()
perm = rng.permutation(iris.target.size)
iris.data, iris.target = shuffle(iris.data, iris.target, random_state=rng)

dataset_doc_path = "/Users/shah/Desktop/Development/d3m/d3m-production/tests-data/datasets/iris_dataset_1/datasetDoc.json"

class TestSKBernoulliNB(unittest.TestCase):

    def test(self):
        classes = np.unique(iris.target)
        dataset = container.Dataset.load(dataset_uri="file://{}".format(dataset_doc_path))
        hyperparams = SKBernoulliNB.Hyperparams.defaults().replace({"use_semantic_types": True})
        # Create the model object
        clf = SKBernoulliNB.SKBernoulliNB(hyperparams=hyperparams)


        hyperparams_class = \
        dataset_to_dataframe.DatasetToDataFramePrimitive.metadata.query()['primitive_code']['class_type_arguments'][
            'Hyperparams']
        primitive = dataset_to_dataframe.DatasetToDataFramePrimitive(hyperparams=hyperparams_class.defaults())
        call_metadata = primitive.produce(inputs=dataset)

        dataframe = call_metadata.value
        column_parser_htperparams = column_parser.Hyperparams.defaults()
        column_parser_primitive = column_parser.ColumnParserPrimitive(hyperparams=column_parser_htperparams)
        parsed_dataframe = column_parser_primitive.produce(inputs=dataframe).value
        train_set = parsed_dataframe
        targets = parsed_dataframe

        clf.set_training_data(inputs=parsed_dataframe, outputs=parsed_dataframe)
        clf.fit()
        output = clf.produce(inputs=parsed_dataframe)

        # Testing get_params() and set_params()
        params = clf.get_params()
        clf.set_params(params=params)
        first_output = clf.produce(inputs=parsed_dataframe)

        # pickle the params and hyperparams
        pickled_params = pickle.dumps(params)
        unpickled_params = pickle.loads(pickled_params)

        pickled_hyperparams = pickle.dumps(hyperparams)
        unpickled_hyperparams = pickle.loads(pickled_hyperparams)

        # Create a new object from pickled params and hyperparams
        new_clf = SKBernoulliNB.SKBernoulliNB(hyperparams=unpickled_hyperparams)
        new_clf.set_params(params=unpickled_params)
        new_output = new_clf.produce(inputs=train_set)

        # Check if outputs match
        assert_frame_equal(first_output.value, output.value)
        assert_frame_equal(new_output.value, output.value)

        # We want to test the running of the code without errors and not the correctness of it
        # since that is assumed to be tested by sklearn
        # assert np.array_equal(classes, clf._clf.classes_)
        print("SUCCESS: Test fit produce on SKBernoulliNB")
        model = pickle.dumps(clf)
        new_clf = pickle.loads(model)
        new_output = new_clf.produce(inputs=train_set)

        assert_frame_equal(first_output.value, output.value)
        assert_frame_equal(new_output.value, output.value)
        print("SUCCESS: Test pickling entire model on SKBernoulliNB")


if __name__ == '__main__':
    unittest.main()
