import unittest
import numpy as np
import pickle
from typing import NamedTuple

from sklearn import datasets
from sklearn.utils import shuffle

from . import SKLabelEncoder
from d3m import container
from pandas.util.testing import assert_frame_equal

# Common random state
rng = np.random.RandomState(0)

# Load the iris dataset and randomly permute it
iris = datasets.load_iris()
perm = rng.permutation(iris.target.size)
iris.data, iris.target = shuffle(iris.data, iris.target, random_state=rng)


class TestSKLabelEncoder(unittest.TestCase):

    def test_fit_iris(self):
        classes = np.unique(iris.target)
        hyperparams = SKLabelEncoder.Hyperparams.defaults()
        clf = SKLabelEncoder.SKLabelEncoder(hyperparams=hyperparams)
        train_set = container.DataFrame(iris.target)
        clf.set_training_data(inputs=train_set)
        clf.fit()
        output = clf.produce(inputs=train_set)
        params = clf.get_params()
        # pickle the params and hyperparams
        pickled_params = pickle.dumps(params)
        unpickled_params = pickle.loads(pickled_params)

        pickled_hyperparams = pickle.dumps(hyperparams)
        unpickled_hyperparams = pickle.loads(pickled_hyperparams)

        # Create a new object from pickled params and hyperparams
        new_clf = SKLabelEncoder.SKLabelEncoder(hyperparams=unpickled_hyperparams)
        new_clf.set_params(params=unpickled_params)
        new_output = new_clf.produce(inputs=train_set)

        assert_frame_equal(output.value, new_output.value)

        hyperparams = SKLabelEncoder.Hyperparams.defaults()
        clf = SKLabelEncoder.SKLabelEncoder(hyperparams=hyperparams)
        train_set = container.DataFrame(iris.data)
        clf.set_training_data(inputs=train_set)
        clf.fit()
        recreate_output = clf.produce(inputs=train_set)
        assert_frame_equal(output.value, recreate_output.value)


        # We want to test the running of the code without errors and not the correctness of it
        # since that is assumed to be tested by sklearn
        # assert()

if __name__ == '__main__':
    unittest.main()
