from typing import Any, Callable, List, Dict, Union, Optional, Sequence, Tuple
from numpy import ndarray
from collections import OrderedDict
from scipy import sparse
import os
import sklearn
import numpy
import typing
import numbers
import decimal

# Custom import commands if any
from sklearn.naive_bayes import BernoulliNB
import common_primitives.utils as common_utils

from d3m.container.numpy import ndarray as d3m_ndarray
from d3m.container import DataFrame as d3m_dataframe
from d3m.metadata import hyperparams, params, base as metadata_base
from d3m import utils
from d3m.primitive_interfaces.base import CallResult, DockerContainer
from d3m.primitive_interfaces.supervised_learning import SupervisedLearnerPrimitiveBase
from d3m.primitive_interfaces.base import ProbabilisticCompositionalityMixin


Inputs = d3m_dataframe
Outputs = d3m_dataframe


class Params(params.Params):
    class_log_prior_: Optional[ndarray]
    feature_log_prob_: Optional[ndarray]
    class_count_: Optional[ndarray]
    feature_count_: Optional[ndarray]
    classes_: Optional[ndarray]
    target_names_: Optional[Sequence[Any]]
    training_indices_: Optional[Sequence[int]]


class Hyperparams(hyperparams.Hyperparams):
    alpha = hyperparams.Hyperparameter[float](
        default=1,
        description='Additive (Laplace/Lidstone) smoothing parameter (0 for no smoothing). ',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )
    binarize = hyperparams.Hyperparameter[Union[float, None]](
        default=0,
        description='Threshold for binarizing (mapping to booleans) of sample features. If None, input is presumed to already consist of binary vectors. ',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )
    fit_prior = hyperparams.Hyperparameter[bool](
        default=True,
        description='Whether to learn class prior probabilities or not. If false, a uniform prior will be used. ',
        semantic_types=['http://schema.org/Boolean', 'https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )
    class_prior = hyperparams.Hyperparameter[Union[ndarray, None]](
        default=None,
        description='Prior probabilities of the classes. If specified the priors are not adjusted according to the data. ',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/TuningParameter']
    )
    use_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to force primitive to operate on. If any specified column cannot be parsed, it is skipped.",
    )
    exclude_columns = hyperparams.Set(
        elements=hyperparams.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to not operate on. Applicable only if \"use_columns\" is not provided.",
    )
    return_result = hyperparams.Enumeration(
        values=['append', 'replace', 'new'],
        default='replace',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Should parsed columns be appended, should they replace original columns, or should only parsed columns be returned?",
    )
    use_semantic_types = hyperparams.UniformBool(
        default=False,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Controls whether semantic_types metadata will be used for filtering columns in input dataframe."
    )
    add_index_columns = hyperparams.UniformBool(
        default=True,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Also include primary index columns if input data has them. Applicable only if \"return_result\" is set to \"new\".",
    )


class SKBernoulliNB(SupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams],
                          ProbabilisticCompositionalityMixin[Inputs, Outputs, Params, Hyperparams]):
    """
    Primitive wrapping for sklearn BernoulliNB
    """

    __author__ = "JPL MARVIN"
    metadata = metadata_base.PrimitiveMetadata({
         "algorithm_types": [metadata_base.PrimitiveAlgorithmType.NAIVE_BAYES_CLASSIFIER, ],
         "name": "sklearn.naive_bayes.BernoulliNB",
         "primitive_family": metadata_base.PrimitiveFamily.CLASSIFICATION,
         "python_path": "d3m.primitives.sklearn_wrap.SKBernoulliNB",
         "source": {'name': 'JPL'},
         "version": "2018.6.20",
         "id": "dfb1004e-02ac-3399-ba57-8a95639312cd",
         'installation': [{'type': metadata_base.PrimitiveInstallationType.PIP,
                           'package_uri': 'git+https://gitlab.com/datadrivendiscovery/sklearn-wrap.git@{git_commit}#egg=sklearn_wrap'.format(
                               git_commit=utils.current_git_commit(os.path.dirname(__file__)),
                            ),
                           }]
    })

    def __init__(self, *,
                 hyperparams: Hyperparams,
                 random_seed: int = 0,
                 docker_containers: Dict[str, DockerContainer] = None) -> None:

        super().__init__(hyperparams=hyperparams, random_seed=random_seed, docker_containers=docker_containers)

        self._clf = BernoulliNB(
            alpha=self.hyperparams['alpha'],
            binarize=self.hyperparams['binarize'],
            fit_prior=self.hyperparams['fit_prior'],
            class_prior=self.hyperparams['class_prior'],
        )
        self._training_inputs = None
        self._training_outputs = None
        self._target_names = None
        self._training_indices = None
        self._fitted = False

    def set_training_data(self, *, inputs: Inputs, outputs: Outputs) -> None:
        self._training_inputs, self._training_indices = self._get_columns_to_produce(inputs, self.hyperparams)
        self._training_outputs, self._target_names = self._get_targets(outputs, self.hyperparams)
        self._fitted = False

    def fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:
        if self._fitted:
            return CallResult(None)

        if self._training_inputs is None or self._training_outputs is None:
            raise ValueError("Missing training data.")
        sk_training_output = d3m_ndarray(self._training_outputs)

        shape = sk_training_output.shape
        if len(shape) == 2 and shape[1] == 1:
            sk_training_output = numpy.ravel(sk_training_output)

        self._clf.fit(self._training_inputs, sk_training_output)
        self._fitted = True

        return CallResult(None)

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:
        sk_inputs = inputs.iloc[:, self._training_indices]
        sk_output = self._clf.predict(sk_inputs)
        if sparse.issparse(sk_output):
            sk_output = sk_output.toarray()
        output = d3m_dataframe(sk_output, generate_metadata=False, source=self)
        output.metadata = inputs.metadata.clear(source=self, for_value=output, generate_metadata=True)
        output.metadata = self._add_target_semantic_types(metadata=output.metadata, target_names=self._target_names, source=self)
        outputs = common_utils.combine_columns(return_result=self.hyperparams['return_result'],
                                               add_index_columns=self.hyperparams['add_index_columns'],
                                               inputs=inputs, column_indices=[], columns_list=[output], source=self)

        return CallResult(outputs)

    def get_params(self) -> Params:
        if not self._fitted:
            raise ValueError("Fit not performed.")
        return Params(
            class_log_prior_=getattr(self._clf, 'class_log_prior_', None),
            feature_log_prob_=getattr(self._clf, 'feature_log_prob_', None),
            class_count_=getattr(self._clf, 'class_count_', None),
            feature_count_=getattr(self._clf, 'feature_count_', None),
            classes_=getattr(self._clf, 'classes_', None),
            training_indices_=self._training_indices,
            target_names_=self._target_names
        )

    def set_params(self, *, params: Params) -> None:
        self._clf.class_log_prior_ = params['class_log_prior_']
        self._clf.feature_log_prob_ = params['feature_log_prob_']
        self._clf.class_count_ = params['class_count_']
        self._clf.feature_count_ = params['feature_count_']
        self._clf.classes_ = params['classes_']
        self._fitted = True
        self._training_indices = params['training_indices_']
        self._target_names = params['target_names_']

    def log_likelihoods(self, *,
                    outputs: Outputs,
                    inputs: Inputs,
                    timeout: float = None,
                    iterations: int = None) -> CallResult[Sequence[float]]:
        inputs = inputs.values  # Get ndarray
        outputs = outputs.values
        return CallResult(self._clf.predict_log_proba(inputs)[:, outputs])

    @classmethod
    def _get_columns_to_produce(cls, inputs: Inputs, hyperparams: Hyperparams):

        inputs_metadata = inputs.metadata

        def can_produce_column(column_index: int) -> bool:
            return cls._can_produce_column(inputs_metadata, column_index, hyperparams)

        columns_to_produce, columns_not_to_produce = common_utils.get_columns_to_use(inputs_metadata,
                                                                                     use_columns=hyperparams['use_columns'],
                                                                                     exclude_columns=hyperparams['exclude_columns'],
                                                                                     can_use_column=can_produce_column)
        return inputs.iloc[:, columns_to_produce], columns_to_produce
        # return columns_to_produce

    @classmethod
    def _can_produce_column(cls, inputs_metadata: metadata_base.DataMetadata, column_index: int, hyperparams: Hyperparams) -> bool:
        column_metadata = inputs_metadata.query((metadata_base.ALL_ELEMENTS, column_index))

        accetped_structural_types = [int, float, numpy.int64, numpy.float64]
        if not utils.is_subclass(column_metadata['structural_type'], typing.Union[float, int, numpy.integer, numpy.float64, numbers.Integral, decimal.Decimal, numbers.Real]):
            return False
        if column_metadata['structural_type'] not in accetped_structural_types:
            return False

        if not hyperparams['use_semantic_types']:
            return True

        semantic_types = column_metadata.get('semantic_types', [])
        if len(semantic_types) == 0:
            return True
        if "https://metadata.datadrivendiscovery.org/types/Attribute" in semantic_types:
            return True

        return False

    @classmethod
    def _get_targets(cls, data: d3m_dataframe, hyperparams: Hyperparams):
        target_names = []
        target_column_indices = []

        metadata = data.metadata
        if not hyperparams['use_semantic_types']:
            return data, target_names
        target_column_indices.extend(metadata.get_columns_with_semantic_type('https://metadata.datadrivendiscovery.org/types/TrueTarget'))
        target_column_indices.extend(metadata.get_columns_with_semantic_type('https://metadata.datadrivendiscovery.org/types/RedactedTarget'))
        target_column_indices.extend(
            metadata.get_columns_with_semantic_type('https://metadata.datadrivendiscovery.org/types/SuggestedTarget'))
        for column_index in target_column_indices:
            if column_index is metadata_base.ALL_ELEMENTS:
                continue
            column_index = typing.cast(metadata_base.SimpleSelectorSegment, column_index)
            column_metadata = metadata.query((metadata_base.ALL_ELEMENTS, column_index))
            target_names.append(column_metadata.get('name', str(column_index)))

        targets = data.iloc[:, target_column_indices]
        return targets, target_names

    @classmethod
    def _add_target_semantic_types(cls, metadata: metadata_base.DataMetadata,
                            source: typing.Any,  target_names: List = None,) -> metadata_base.DataMetadata:
        for column_index in range(metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']):
            metadata = metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, column_index),
                                                  'https://metadata.datadrivendiscovery.org/types/Target',
                                                  source=source)
            metadata = metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, column_index),
                                                  'https://metadata.datadrivendiscovery.org/types/PredictedTarget',
                                                  source=source)
            if target_names:
                metadata = metadata.update((metadata_base.ALL_ELEMENTS, column_index), {
                    'name': target_names[column_index],
                }, source=source)
        return metadata


SKBernoulliNB.__doc__ = BernoulliNB.__doc__
