"""
File for testing if the right hyperparameters and parameters are present in the primitive.

`params_check` detects if there's hyperparameter or parameter that shouldn't be there, and if there's
hyperparameter or parameter missing from the primitive.

It does that by comparing parameters from __getstate__ function (sklearn) with get_params function
merged with hyperparameters (wrapper primitive).

It also checks if parameters have the right type. If parameter should be hyperparameter (or vise versa),
an error is thrown. That is done by comparing our hyperparameters with parameters retrieved from the JSON
of the primitive.

Test should be present in all UNIT tests except for transformers.

Attributes
----------
primitive_jsons_path : str
only_in_sklearn
    Parameters and hyperparameters in sklearn that are ignored. They can't
    be present in wrapper primitive.
only_in_wrapper_hyperparams
    Hyperparameters in wrapper that are ignored.
only_in_wrapper_params
    Parameters in wrapper that are ignored.
deprecated_hyperparams
    Deprecated hyperparameters, effect of which can be reproduced by other hyperparameters.
    They can't be in wrapper primitive.
"""

import json
import os
import typing
from importlib import import_module
from inspect import getfullargspec

from sklearn.svm import SVR

primitive_jsons_path = 'tests/resources/primitive-jsons'
only_in_sklearn = {
    '_sklearn_version',
    'random_state',  # standard constructor argument
    'copy',  # is handled by the primitive itself
    'verbose',  # standard constructor argument
    'compute_score',
    'copy_X',  # is handled by the primitive itself
    'estimator',  # future hyperparameter
    'class_prior',  # future hyperparameter
    'priors',  # future hyperparameter
    'preprocessor',  # future hyperparameter
    'pooling_func',  # future hyperparameter
    'tokenizer',  # future hyperparameter
}
only_in_wrapper_hyperparams = {
    'add_index_columns',
    'exclude_columns',
    'return_result',
    'use_columns',
    'use_semantic_types',
}
only_in_wrapper_params = {
    'target_names_',
    'training_indices_',
}
deprecated_hyperparams = {
    'min_impurity_split',
}


def write_params(wicked_params: set, problem: str, *, source_class: typing.ClassVar = None, all_params: set = None) -> str:
    """
    Transforms parameters ``wicked_params`` into more human readable form.
    
    If ``source_class`` is present, each parameter is given its type.
    
    If ``all_params`` is present, ``wicked_params`` are removed from ``all_params``. That's
    used if you want that some parameters are reported only once.
    
    Parameters
    ----------
    wicked_params : set
        Parameters that are to be merged into one string.
    problem : str
        Description of why parameter is reported and what's possibly a solution.
    source_class : typing.ClassVar
        Class that ``wicked_params`` belong to.
    all_params : set
        Set, from which ``wicked_params`` were selected from.

    Returns
    -------
        String, concatenation of all parameters.
    """

    if all_params is not None:
        all_params.difference_update(wicked_params)

    if source_class is None:
        return ''.join(["  - `{0}` {1}.\n".format(param, problem) for param in sorted(wicked_params)])
    return ''.join(["  - `{0}` {1} {2}.\n".format(param, type(getattr(source_class, param)), problem) for param in sorted(wicked_params)])


def check(errors: str, name: str) -> None:
    """
    Throws an error if ``errors`` has length greater than 0.

    Parameters
    ----------
    errors : str
        Errors in a string.
    name : str
        Name of the wrapper primitive.
    """

    assert len(errors) == 0, "\n\nError(s) in SK{0} primitive:\n" \
                             "{1}" \
                             "If you think this isn't the case, manipulate with `only_in_wrapper_hyperparams`, " \
                             "`only_in_wrapper_params`, `only_in_sklearn`, and `deprecated_hyperparams` lists." \
        .format(name, errors)


def params_check(wrapper, input_data=None, output_data=None, *, use_semantic_types: bool = False):
    """
    Collects all errors and reports them once, so user can fix them in one step.

    Parameters
    ----------
    wrapper : class
        Wrapper primitive.
    input_data
        Input to the fit methods.
    output_data
        Output to the fit methods.
    use_semantic_types : bool
        Whether `use_semantic_types` hyperparameter should be True or False.
    """

    # Placeholder for errors.
    errors = ''

    # Getting instance of the wrapper primitive.
    hyperparams = wrapper.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams'].defaults()
    if use_semantic_types:
        hyperparams = hyperparams.replace({"use_semantic_types": True})
    wrapper = wrapper(hyperparams=hyperparams)

    # Fitting.
    kwargs = {}
    if input_data is not None:
        kwargs['inputs'] = input_data
    if output_data is not None:
        kwargs['outputs'] = output_data
    wrapper.set_training_data(**kwargs)
    wrapper.fit()

    # Getting instance of the sklearn class.
    module = wrapper.metadata.query()['name']
    name = module[module.rfind('.') + 1:]
    sklearn = getattr(import_module(module[:module.rfind('.')]), name)

    # Setting all flags to True, so '__getstate__' function contains as many parameters as possible.
    kwargs = {} if name != 'RFE' else {'estimator': SVR(kernel='linear')}
    args = getfullargspec(sklearn)
    for i, arg in enumerate(args[3] if args[3] is not None else []):
        if arg is False and args[0][i + 1] not in only_in_sklearn.union({'warm_start'}):
            kwargs[args[0][i + 1]] = True

    # If subsample is 1, '__getstate__' function doesn't have some parameters.
    if 'subsample' in args[0]:
        kwargs['subsample'] = .9

    sklearn = sklearn(**kwargs)

    # Fitting.
    args = []
    if input_data is not None:
        args.append(wrapper._training_inputs)
    if output_data is not None:
        args.append(wrapper._training_outputs)
    # Vectorizers need to have `list` as an input, not DataSet.
    # TODO: Remove next two lines after https://gitlab.com/datadrivendiscovery/sklearn-wrap/issues/108 is fixed.
    if name.endswith('Vectorizer'):
        args = [input_data[0]]
    sklearn.fit(*args)

    # Getting parameters.
    wrapper_params = set(wrapper.get_params().keys() if wrapper.get_params() is not None else []).difference(only_in_wrapper_params)
    wrapper_hyperparams = set(hyperparams.keys()).difference(only_in_wrapper_hyperparams)
    sklearn_all = set(sklearn.__getstate__().keys()).difference(only_in_sklearn).difference(deprecated_hyperparams)
    sklearn_hyperparams = {}
    for file in os.listdir(primitive_jsons_path):
        primitive = json.load(open(os.path.join(primitive_jsons_path, file)))
        if primitive['name'].endswith(name[2:]):
            sklearn_hyperparams = {param['name'] for param in primitive.get('parameters', [])}
            break
    sklearn_hyperparams.difference_update(only_in_sklearn.union(deprecated_hyperparams))

    # Ensuring that we don't have `only_in_*` parameters anywhere.
    errors += write_params(sklearn_all.intersection(only_in_wrapper_params),
                           "is in sklearn and in `only_in_wrapper_params`, which shouldn't happen")
    errors += write_params(sklearn_all.intersection(only_in_wrapper_hyperparams),
                           "is in sklearn and in `only_in_wrapper_hyperparams`, which shouldn't happen")
    errors += write_params(wrapper_params.intersection(only_in_sklearn),
                           "shouldn't be in `Params` (it's in `only_in_sklearn`)")
    errors += write_params(wrapper_hyperparams.intersection(only_in_sklearn),
                           "shouldn't be hyperparameter (it's in `only_in_sklearn`)")

    # We want that that above errors are fixed before proceeding, otherwise we might produce misleading errors.
    check(errors, name)

    # Looking for params that are properties.
    properties = {param for param in wrapper_params.union(wrapper_hyperparams) if hasattr(type(sklearn), param) and isinstance(getattr(type(sklearn), param), property)}
    errors += write_params(wrapper_params.intersection(properties),
                           "shouldn't be in `Params` (it's property)",
                           all_params=wrapper_params)

    # Looking for deprecated hyperparams.
    errors += write_params(wrapper_hyperparams.intersection(deprecated_hyperparams),
                           "shouldn't be hyperparameter (it's deprecated hyperparameter)",
                           all_params=wrapper_hyperparams)

    # Looking for params that are actually hyperparams.
    errors += write_params(wrapper_params.intersection(wrapper_hyperparams.union(sklearn_hyperparams)),
                           "shouldn't be in `Params` (it's hyperparameter)",
                           all_params=wrapper_params)

    # It's okay if hyperparameter is property.
    wrapper_hyperparams.difference_update(properties)

    # Looking for parameters and hyperparameters that should be in wrapper primitive.
    missing = sklearn_all.difference(wrapper_params).difference(wrapper_hyperparams)
    errors += write_params(missing.intersection(sklearn_hyperparams),
                           "should be hyperparameter (it's in __getstate__ function of sklearn class)",
                           all_params=missing,
                           source_class=sklearn)
    errors += write_params(missing,
                           "should be added to the primitive (it's either missing parameter or missing hyperparameter)",
                           source_class=sklearn)

    # Looking for parameters and hyperparameters that shouldn't be in wrapper primitive.
    errors += write_params(wrapper_params.difference(sklearn_all),
                           "shouldn't be in `Params` (it isn't in __getstate__ function of sklearn class)")
    errors += write_params(wrapper_hyperparams.difference(sklearn_all),
                           "shouldn't be hyperparameter (it isn't in __getstate__ function of sklearn class)")

    check(errors, name)
