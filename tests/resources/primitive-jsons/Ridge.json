{
  "is_class": true, 
  "library": "sklearn", 
  "compute_resources": {}, 
  "common_name": "Ridge", 
  "id": "7168f3e4-9dab-30da-8e7a-2d5555866cc3", 
  "category": "linear_model.ridge", 
  "source_code_unanalyzed": "https://github.com/scikit-learn/scikit-learn/blob/0.18.X/sklearn/linear_model/ridge.py#L494", 
  "parameters": [
    {
      "shape": "n_targets", 
      "type": "float, array-like", 
      "name": "alpha", 
      "description": "Regularization strength; must be a positive float. Regularization improves the conditioning of the problem and reduces the variance of the estimates. Larger values specify stronger regularization. Alpha corresponds to ``C^-1`` in other linear models such as LogisticRegression or LinearSVC. If an array is passed, penalties are assumed to be specific to the targets. Hence they must correspond in number.  copy_X : boolean, optional, default True If True, X will be copied; else, it may be overwritten. "
    }, 
    {
      "type": "boolean", 
      "name": "fit_intercept", 
      "description": "Whether to calculate the intercept for this model. If set to false, no intercept will be used in calculations (e.g. data is expected to be already centered). "
    }, 
    {
      "type": "int", 
      "optional": "true", 
      "name": "max_iter", 
      "description": "Maximum number of iterations for conjugate gradient solver. For 'sparse_cg' and 'lsqr' solvers, the default value is determined by scipy.sparse.linalg. For 'sag' solver, the default value is 1000. "
    }, 
    {
      "type": "boolean", 
      "optional": "true", 
      "name": "normalize", 
      "description": "If True, the regressors X will be normalized before regression. This parameter is ignored when `fit_intercept` is set to False. When the regressors are normalized, note that this makes the hyperparameters learnt more robust and almost independent of the number of samples. The same property is not valid for standardized data. However, if you wish to standardize, please use `preprocessing.StandardScaler` before calling `fit` on an estimator with `normalize=False`. "
    }, 
    {
      "type": "'auto', 'svd', 'cholesky', 'lsqr', 'sparse_cg', 'sag'", 
      "name": "solver", 
      "description": "Solver to use in the computational routines:  - 'auto' chooses the solver automatically based on the type of data.  - 'svd' uses a Singular Value Decomposition of X to compute the Ridge coefficients. More stable for singular matrices than 'cholesky'.  - 'cholesky' uses the standard scipy.linalg.solve function to obtain a closed-form solution.  - 'sparse_cg' uses the conjugate gradient solver as found in scipy.sparse.linalg.cg. As an iterative algorithm, this solver is more appropriate than 'cholesky' for large-scale data (possibility to set `tol` and `max_iter`).  - 'lsqr' uses the dedicated regularized least-squares routine scipy.sparse.linalg.lsqr. It is the fastest but may not be available in old scipy versions. It also uses an iterative procedure.  - 'sag' uses a Stochastic Average Gradient descent. It also uses an iterative procedure, and is often faster than other solvers when both n_samples and n_features are large. Note that 'sag' fast convergence is only guaranteed on features with approximately the same scale. You can preprocess the data with a scaler from sklearn.preprocessing.  All last four solvers support both dense and sparse data. However, only 'sag' supports sparse input when `fit_intercept` is True.  .. versionadded:: 0.17 Stochastic Average Gradient descent solver. "
    }, 
    {
      "type": "float", 
      "name": "tol", 
      "description": "Precision of the solution. "
    }, 
    {
      "type": "int", 
      "name": "random_state", 
      "description": "The seed of the pseudo random number generator to use when shuffling the data. Used only in 'sag' solver.  .. versionadded:: 0.17 *random_state* to support Stochastic Average Gradient. "
    }
  ], 
  "tags": [
    "linear_model", 
    "ridge"
  ], 
  "common_name_unanalyzed": "Ridge", 
  "schema_version": 1.0, 
  "languages": [
    "python2.7"
  ], 
  "version": "0.18.1", 
  "build": [
    {
      "type": "pip", 
      "package": "scikit-learn"
    }
  ], 
  "description": "\"Linear least squares with l2 regularization.\n\nThis model solves a regression model where the loss function is\nthe linear least squares function and regularization is given by\nthe l2-norm. Also known as Ridge Regression or Tikhonov regularization.\nThis estimator has built-in support for multi-variate regression\n(i.e., when y is a 2d-array of shape [n_samples, n_targets]).\n\nRead more in the :ref:`User Guide <ridge_regression>`.\n", 
  "methods_available": [
    {
      "id": "sklearn.linear_model.ridge.Ridge.decision_function", 
      "returns": {
        "shape": "n_samples,", 
        "type": "array", 
        "name": "C", 
        "description": "Returns predicted values. '"
      }, 
      "description": "'DEPRECATED:  and will be removed in 0.19.\n\nDecision function of the linear model.\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "array-like, sparse matrix", 
          "name": "X", 
          "description": "Samples. "
        }
      ], 
      "name": "decision_function"
    }, 
    {
      "id": "sklearn.linear_model.ridge.Ridge.fit", 
      "returns": {
        "type": "returns", 
        "name": "self", 
        "description": "'"
      }, 
      "description": "'Fit Ridge regression model\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "array-like, sparse matrix", 
          "name": "X", 
          "description": "Training data "
        }, 
        {
          "shape": "n_samples", 
          "type": "array-like", 
          "name": "y", 
          "description": "Target values "
        }, 
        {
          "shape": "n_samples", 
          "type": "float", 
          "name": "sample_weight", 
          "description": "Individual weights for each sample "
        }
      ], 
      "name": "fit"
    }, 
    {
      "id": "sklearn.linear_model.ridge.Ridge.get_params", 
      "returns": {
        "type": "mapping", 
        "name": "params", 
        "description": "Parameter names mapped to their values. '"
      }, 
      "description": "'Get parameters for this estimator.\n", 
      "parameters": [
        {
          "type": "boolean", 
          "optional": "true", 
          "name": "deep", 
          "description": "If True, will return the parameters for this estimator and contained subobjects that are estimators. "
        }
      ], 
      "name": "get_params"
    }, 
    {
      "id": "sklearn.linear_model.ridge.Ridge.predict", 
      "returns": {
        "shape": "n_samples,", 
        "type": "array", 
        "name": "C", 
        "description": "Returns predicted values. '"
      }, 
      "description": "'Predict using the linear model\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "array-like, sparse matrix", 
          "name": "X", 
          "description": "Samples. "
        }
      ], 
      "name": "predict"
    }, 
    {
      "id": "sklearn.linear_model.ridge.Ridge.score", 
      "returns": {
        "type": "float", 
        "name": "score", 
        "description": "R^2 of self.predict(X) wrt. y. '"
      }, 
      "description": "'Returns the coefficient of determination R^2 of the prediction.\n\nThe coefficient R^2 is defined as (1 - u/v), where u is the regression\nsum of squares ((y_true - y_pred) ** 2).sum() and v is the residual\nsum of squares ((y_true - y_true.mean()) ** 2).sum().\nBest possible score is 1.0 and it can be negative (because the\nmodel can be arbitrarily worse). A constant model that always\npredicts the expected value of y, disregarding the input features,\nwould get a R^2 score of 0.0.\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "array-like", 
          "name": "X", 
          "description": "Test samples. "
        }, 
        {
          "shape": "n_samples", 
          "type": "array-like", 
          "name": "y", 
          "description": "True values for X. "
        }, 
        {
          "type": "array-like", 
          "shape": "n_samples", 
          "optional": "true", 
          "name": "sample_weight", 
          "description": "Sample weights. "
        }
      ], 
      "name": "score"
    }, 
    {
      "id": "sklearn.linear_model.ridge.Ridge.set_params", 
      "returns": {
        "name": "self", 
        "description": "\""
      }, 
      "description": "\"Set the parameters of this estimator.\n\nThe method works on simple estimators as well as on nested objects\n(such as pipelines). The latter have parameters of the form\n``<component>__<parameter>`` so that it's possible to update each\ncomponent of a nested object.\n", 
      "parameters": [], 
      "name": "set_params"
    }
  ], 
  "learning_type": [
    "supervised"
  ], 
  "task_type": [
    "modeling"
  ], 
  "source_code": "https://github.com/scikit-learn/scikit-learn/blob/0.18.X/sklearn/linear_model/ridge.py#L494", 
  "category_unanalyzed": "linear_model.ridge", 
  "name": "sklearn.linear_model.ridge.Ridge", 
  "team": "jpl", 
  "attributes": [
    {
      "shape": "n_features,", 
      "type": "array", 
      "name": "coef_", 
      "description": "Weight vector(s). "
    }, 
    {
      "shape": "n_targets,", 
      "type": "float", 
      "name": "intercept_", 
      "description": "Independent term in decision function. Set to 0.0 if ``fit_intercept = False``. "
    }, 
    {
      "shape": "n_targets,", 
      "type": "array", 
      "name": "n_iter_", 
      "description": "Actual number of iterations for each target. Available only for sag and lsqr solvers. Other solvers will return None.  .. versionadded:: 0.17  See also -------- RidgeClassifier, RidgeCV, :class:`sklearn.kernel_ridge.KernelRidge` "
    }
  ]
}