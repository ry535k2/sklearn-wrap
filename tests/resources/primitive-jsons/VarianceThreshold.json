{
  "name": "sklearn.feature_selection.variance_threshold.VarianceThreshold",
  "id": "10b2db925800f790e94909a04f1b0e09",
  "common_name": "VarianceThreshold",
  "is_class": true,
  "tags": [
    "feature_selection",
    "variance_threshold"
  ],
  "version": "0.19.1",
  "source_code": "https://github.com/scikit-learn/scikit-learn/blob/0.19.1/sklearn/feature_selection/variance_threshold.py#L12",
  "parameters": [
    {
      "type": "float",
      "optional": "true",
      "name": "threshold",
      "description": "Features with a training-set variance lower than this threshold will be removed. The default is to keep all features with non-zero variance, i.e. remove the features that have the same value in all samples. "
    }
  ],
  "attributes": [
    {
      "type": "array",
      "shape": "n_features,",
      "name": "variances_",
      "description": "Variances of individual features. "
    }
  ],
  "description": "'Feature selector that removes all low-variance features.\n\nThis feature selection algorithm looks only at the features (X), not the\ndesired outputs (y), and can thus be used for unsupervised learning.\n\nRead more in the :ref:`User Guide <variance_threshold>`.\n",
  "methods_available": [
    {
      "name": "fit",
      "id": "sklearn.feature_selection.variance_threshold.VarianceThreshold.fit",
      "parameters": [
        {
          "type": "array-like, sparse matrix",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "Sample vectors from which to compute variances. "
        },
        {
          "type": "any",
          "name": "y",
          "description": "Ignored. This parameter exists only for compatibility with sklearn.pipeline.Pipeline. "
        }
      ],
      "description": "'Learn empirical variances from X.\n",
      "returns": {
        "name": "self",
        "description": "'"
      }
    },
    {
      "name": "fit_transform",
      "id": "sklearn.feature_selection.variance_threshold.VarianceThreshold.fit_transform",
      "parameters": [
        {
          "type": "numpy",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "Training set. "
        },
        {
          "type": "numpy",
          "shape": "n_samples",
          "name": "y",
          "description": "Target values. "
        }
      ],
      "description": "'Fit to data, then transform it.\n\nFits transformer to X and y with optional parameters fit_params\nand returns a transformed version of X.\n",
      "returns": {
        "type": "numpy",
        "shape": "n_samples, n_features_new",
        "name": "X_new",
        "description": "Transformed array.  '"
      }
    },
    {
      "name": "get_params",
      "id": "sklearn.feature_selection.variance_threshold.VarianceThreshold.get_params",
      "parameters": [
        {
          "type": "boolean",
          "optional": "true",
          "name": "deep",
          "description": "If True, will return the parameters for this estimator and contained subobjects that are estimators. "
        }
      ],
      "description": "'Get parameters for this estimator.\n",
      "returns": {
        "type": "mapping",
        "name": "params",
        "description": "Parameter names mapped to their values. '"
      }
    },
    {
      "name": "get_support",
      "id": "sklearn.feature_selection.variance_threshold.VarianceThreshold.get_support",
      "parameters": [
        {
          "type": "boolean",
          "name": "indices",
          "description": "If True, the return value will be an array of integers, rather than a boolean mask. "
        }
      ],
      "description": "'\nGet a mask, or integer index, of the features selected\n",
      "returns": {
        "type": "array",
        "name": "support",
        "description": "An index that selects the retained features from a feature vector. If `indices` is False, this is a boolean array of shape [# input features], in which an element is True iff its corresponding feature is selected for retention. If `indices` is True, this is an integer array of shape [# output features] whose values are indices into the input feature vector. '"
      }
    },
    {
      "name": "inverse_transform",
      "id": "sklearn.feature_selection.variance_threshold.VarianceThreshold.inverse_transform",
      "parameters": [
        {
          "type": "array",
          "shape": "n_samples, n_selected_features",
          "name": "X",
          "description": "The input samples. "
        }
      ],
      "description": "'\nReverse the transformation operation\n",
      "returns": {
        "type": "array",
        "shape": "n_samples, n_original_features",
        "name": "X_r",
        "description": "`X` with columns of zeros inserted where features would have been removed by `transform`. '"
      }
    },
    {
      "name": "set_params",
      "id": "sklearn.feature_selection.variance_threshold.VarianceThreshold.set_params",
      "parameters": [],
      "description": "\"Set the parameters of this estimator.\n\nThe method works on simple estimators as well as on nested objects\n(such as pipelines). The latter have parameters of the form\n``<component>__<parameter>`` so that it's possible to update each\ncomponent of a nested object.\n",
      "returns": {
        "name": "self",
        "description": "\""
      }
    },
    {
      "name": "transform",
      "id": "sklearn.feature_selection.variance_threshold.VarianceThreshold.transform",
      "parameters": [
        {
          "type": "array",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "The input samples. "
        }
      ],
      "description": "'Reduce X to the selected features.\n",
      "returns": {
        "type": "array",
        "shape": "n_samples, n_selected_features",
        "name": "X_r",
        "description": "The input samples with only the selected features. '"
      }
    }
  ]
}