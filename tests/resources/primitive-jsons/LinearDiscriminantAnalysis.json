{
  "handles_regression": false,
  "is_class": true,
  "library": "sklearn",
  "compute_resources": {},
  "common_name": "Linear Discriminant Analysis",
  "id": "5a4ce072-967d-3998-be7e-83cd9a4c5f2f",
  "handles_classification": true,
  "category": "discriminant_analysis",
  "source_code_unanalyzed": "https://github.com/scikit-learn/scikit-learn/blob/0.18.X/sklearn/discriminant_analysis.py#L130",
  "parameters": [
    {
      "type": "string",
      "optional": "true",
      "name": "solver",
      "description": "Solver to use, possible values: - 'svd': Singular value decomposition (default). Does not compute the covariance matrix, therefore this solver is recommended for data with a large number of features. - 'lsqr': Least squares solution, can be combined with shrinkage. - 'eigen': Eigenvalue decomposition, can be combined with shrinkage. "
    },
    {
      "type": "string",
      "optional": "true",
      "name": "shrinkage",
      "description": "Shrinkage parameter, possible values: - None: no shrinkage (default). - 'auto': automatic shrinkage using the Ledoit-Wolf lemma. - float between 0 and 1: fixed shrinkage parameter.  Note that shrinkage works only with 'lsqr' and 'eigen' solvers. "
    },
    {
      "type": "array",
      "shape": "n_classes,",
      "optional": "true",
      "name": "priors",
      "description": "Class priors. "
    },
    {
      "type": "int",
      "optional": "true",
      "name": "n_components",
      "description": "Number of components (< n_classes - 1) for dimensionality reduction. "
    },
    {
      "type": "bool",
      "optional": "true",
      "name": "store_covariance",
      "description": "Additionally compute class covariance matrix (default False).  .. versionadded:: 0.17 "
    },
    {
      "type": "float",
      "optional": "true",
      "name": "tol",
      "description": "Threshold used for rank estimation in SVD solver.  .. versionadded:: 0.17 "
    }
  ],
  "input_type": [
    "DENSE",
    "UNSIGNED_DATA"
  ],
  "methods_available": [
    {
      "id": "sklearn.discriminant_analysis.LinearDiscriminantAnalysis.decision_function",
      "returns": {
        "name": "array, shape=(n_samples,) if n_classes == 2 else (n_samples, n_classes)",
        "description": "Confidence scores per (sample, class) combination. In the binary case, confidence score for self.classes_[1] where >0 means this class would be predicted. '"
      },
      "description": "'Predict confidence scores for samples.\n\nThe confidence score for a sample is the signed distance of that\nsample to the hyperplane.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like, sparse matrix",
          "name": "X",
          "description": "Samples. "
        }
      ],
      "name": "decision_function"
    },
    {
      "id": "sklearn.discriminant_analysis.LinearDiscriminantAnalysis.fit",
      "description": "'Fit LinearDiscriminantAnalysis model according to the given\ntraining data and parameters.\n\n.. versionchanged:: 0.17\nDeprecated *store_covariance* have been moved to main constructor.\n\n.. versionchanged:: 0.17\nDeprecated *tol* have been moved to main constructor.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like",
          "name": "X",
          "description": "Training data. "
        },
        {
          "shape": "n_samples,",
          "type": "array",
          "name": "y",
          "description": "Target values. '"
        }
      ],
      "name": "fit"
    },
    {
      "id": "sklearn.discriminant_analysis.LinearDiscriminantAnalysis.fit_transform",
      "returns": {
        "shape": "n_samples, n_features_new",
        "type": "numpy",
        "name": "X_new",
        "description": "Transformed array.  '"
      },
      "description": "'Fit to data, then transform it.\n\nFits transformer to X and y with optional parameters fit_params\nand returns a transformed version of X.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "numpy",
          "name": "X",
          "description": "Training set. "
        },
        {
          "shape": "n_samples",
          "type": "numpy",
          "name": "y",
          "description": "Target values. "
        }
      ],
      "name": "fit_transform"
    },
    {
      "id": "sklearn.discriminant_analysis.LinearDiscriminantAnalysis.get_params",
      "returns": {
        "type": "mapping",
        "name": "params",
        "description": "Parameter names mapped to their values. '"
      },
      "description": "'Get parameters for this estimator.\n",
      "parameters": [
        {
          "type": "boolean",
          "optional": "true",
          "name": "deep",
          "description": "If True, will return the parameters for this estimator and contained subobjects that are estimators. "
        }
      ],
      "name": "get_params"
    },
    {
      "id": "sklearn.discriminant_analysis.LinearDiscriminantAnalysis.predict",
      "returns": {
        "shape": "n_samples",
        "type": "array",
        "name": "C",
        "description": "Predicted class label per sample. '"
      },
      "description": "'Predict class labels for samples in X.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like, sparse matrix",
          "name": "X",
          "description": "Samples. "
        }
      ],
      "name": "predict"
    },
    {
      "id": "sklearn.discriminant_analysis.LinearDiscriminantAnalysis.predict_log_proba",
      "returns": {
        "shape": "n_samples, n_classes",
        "type": "array",
        "name": "C",
        "description": "Estimated log probabilities. '"
      },
      "description": "'Estimate log probability.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like",
          "name": "X",
          "description": "Input data. "
        }
      ],
      "name": "predict_log_proba"
    },
    {
      "id": "sklearn.discriminant_analysis.LinearDiscriminantAnalysis.predict_proba",
      "returns": {
        "shape": "n_samples, n_classes",
        "type": "array",
        "name": "C",
        "description": "Estimated probabilities. '"
      },
      "description": "'Estimate probability.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like",
          "name": "X",
          "description": "Input data. "
        }
      ],
      "name": "predict_proba"
    },
    {
      "id": "sklearn.discriminant_analysis.LinearDiscriminantAnalysis.score",
      "returns": {
        "type": "float",
        "name": "score",
        "description": "Mean accuracy of self.predict(X) wrt. y.  '"
      },
      "description": "'Returns the mean accuracy on the given test data and labels.\n\nIn multi-label classification, this is the subset accuracy\nwhich is a harsh metric since you require for each sample that\neach label set be correctly predicted.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like",
          "name": "X",
          "description": "Test samples. "
        },
        {
          "shape": "n_samples",
          "type": "array-like",
          "name": "y",
          "description": "True labels for X. "
        },
        {
          "type": "array-like",
          "shape": "n_samples",
          "optional": "true",
          "name": "sample_weight",
          "description": "Sample weights. "
        }
      ],
      "name": "score"
    },
    {
      "id": "sklearn.discriminant_analysis.LinearDiscriminantAnalysis.set_params",
      "returns": {
        "name": "self",
        "description": "\""
      },
      "description": "\"Set the parameters of this estimator.\n\nThe method works on simple estimators as well as on nested objects\n(such as pipelines). The latter have parameters of the form\n``<component>__<parameter>`` so that it's possible to update each\ncomponent of a nested object.\n",
      "parameters": [],
      "name": "set_params"
    },
    {
      "id": "sklearn.discriminant_analysis.LinearDiscriminantAnalysis.transform",
      "returns": {
        "shape": "n_samples, n_components",
        "type": "array",
        "name": "X_new",
        "description": "Transformed data. '"
      },
      "description": "'Project data to maximize class separation.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like",
          "name": "X",
          "description": "Input data. "
        }
      ],
      "name": "transform"
    }
  ],
  "common_name_unanalyzed": "Linear Discriminant Analysis",
  "schema_version": 1.0,
  "languages": [
    "python2.7"
  ],
  "version": "0.18.1",
  "build": [
    {
      "type": "pip",
      "package": "scikit-learn"
    }
  ],
  "handles_multiclass": true,
  "description": "\"Linear Discriminant Analysis\n\nA classifier with a linear decision boundary, generated by fitting class\nconditional densities to the data and using Bayes' rule.\n\nThe model fits a Gaussian density to each class, assuming that all classes\nshare the same covariance matrix.\n\nThe fitted model can also be used to reduce the dimensionality of the input\nby projecting it to the most discriminative directions.\n\n.. versionadded:: 0.17\n*LinearDiscriminantAnalysis*.\n\nRead more in the :ref:`User Guide <lda_qda>`.\n",
  "tags": [
    "discriminant_analysis"
  ],
  "learning_type": [
    "supervised"
  ],
  "task_type": [
    "modeling",
    "feature extraction"
  ],
  "output_type": [
    "PREDICTIONS"
  ],
  "source_code": "https://github.com/scikit-learn/scikit-learn/blob/0.18.X/sklearn/discriminant_analysis.py#L130",
  "category_unanalyzed": "discriminant_analysis",
  "name": "sklearn.discriminant_analysis.LinearDiscriminantAnalysis",
  "handles_multilabel": true,
  "is_deterministic": true,
  "team": "jpl",
  "attributes": [
    {
      "shape": "n_features,",
      "type": "array",
      "name": "coef_",
      "description": "Weight vector(s). "
    },
    {
      "shape": "n_features,",
      "type": "array",
      "name": "intercept_",
      "description": "Intercept term. "
    },
    {
      "shape": "n_features, n_features",
      "type": "array-like",
      "name": "covariance_",
      "description": "Covariance matrix (shared by all classes). "
    },
    {
      "shape": "n_components,",
      "type": "array",
      "name": "explained_variance_ratio_",
      "description": "Percentage of variance explained by each of the selected components. If ``n_components`` is not set then all components are stored and the sum of explained variances is equal to 1.0. Only available when eigen or svd solver is used. "
    },
    {
      "shape": "n_classes, n_features",
      "type": "array-like",
      "name": "means_",
      "description": "Class means. "
    },
    {
      "shape": "n_classes,",
      "type": "array-like",
      "name": "priors_",
      "description": "Class priors (sum to 1). "
    },
    {
      "shape": "rank, n_classes - 1",
      "type": "array-like",
      "name": "scalings_",
      "description": "Scaling of the features in the space spanned by the class centroids. "
    },
    {
      "shape": "n_features,",
      "type": "array-like",
      "name": "xbar_",
      "description": "Overall mean. "
    },
    {
      "shape": "n_classes,",
      "type": "array-like",
      "name": "classes_",
      "description": "Unique class labels.  See also -------- sklearn.discriminant_analysis.QuadraticDiscriminantAnalysis: Quadratic Discriminant Analysis  Notes ----- The default solver is 'svd'. It can perform both classification and transform, and it does not rely on the calculation of the covariance matrix. This can be an advantage in situations where the number of features is large. However, the 'svd' solver cannot be used with shrinkage.  The 'lsqr' solver is an efficient algorithm that only works for classification. It supports shrinkage.  The 'eigen' solver is based on the optimization of the between class scatter to within class scatter ratio. It can be used for both classification and transform, and it supports shrinkage. However, the 'eigen' solver needs to compute the covariance matrix, so it might not be suitable for situations with a high number of features. "
    }
  ]
}
