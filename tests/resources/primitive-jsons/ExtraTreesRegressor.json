{
  "handles_regression": true,
  "is_class": true,
  "library": "sklearn",
  "compute_resources": {},
  "common_name": "Extra Trees Regressor",
  "id": "4bcb5d8d-e2e7-3ac2-b057-2ae6fca815ab",
  "handles_classification": false,
  "category": "ensemble.forest",
  "source_code_unanalyzed": "https://github.com/scikit-learn/scikit-learn/blob/0.18.X/sklearn/ensemble/forest.py#L1337",
  "parameters": [
    {
      "default": "10",
      "type": "integer",
      "optional": "true",
      "name": "n_estimators",
      "description": "The number of trees in the forest. "
    },
    {
      "default": "\"mse\"",
      "type": "string",
      "optional": "true",
      "name": "criterion",
      "description": "The function to measure the quality of a split. Supported criteria are \"mse\" for the mean squared error, which is equal to variance reduction as feature selection criterion, and \"mae\" for the mean absolute error.  .. versionadded:: 0.18 Mean Absolute Error (MAE) criterion. "
    },
    {
      "default": "\"auto\"",
      "type": "int",
      "optional": "true",
      "name": "max_features",
      "description": "The number of features to consider when looking for the best split:  - If int, then consider `max_features` features at each split. - If float, then `max_features` is a percentage and `int(max_features * n_features)` features are considered at each split. - If \"auto\", then `max_features=n_features`. - If \"sqrt\", then `max_features=sqrt(n_features)`. - If \"log2\", then `max_features=log2(n_features)`. - If None, then `max_features=n_features`.  Note: the search for a split does not stop until at least one valid partition of the node samples is found, even if it requires to effectively inspect more than ``max_features`` features. "
    },
    {
      "default": "None",
      "type": "integer",
      "optional": "true",
      "name": "max_depth",
      "description": "The maximum depth of the tree. If None, then nodes are expanded until all leaves are pure or until all leaves contain less than min_samples_split samples. "
    },
    {
      "default": "2",
      "type": "int",
      "optional": "true",
      "name": "min_samples_split",
      "description": "The minimum number of samples required to split an internal node:  - If int, then consider `min_samples_split` as the minimum number. - If float, then `min_samples_split` is a percentage and `ceil(min_samples_split * n_samples)` are the minimum number of samples for each split.  .. versionchanged:: 0.18 Added float values for percentages. "
    },
    {
      "default": "1",
      "type": "int",
      "optional": "true",
      "name": "min_samples_leaf",
      "description": "The minimum number of samples required to be at a leaf node:  - If int, then consider `min_samples_leaf` as the minimum number. - If float, then `min_samples_leaf` is a percentage and `ceil(min_samples_leaf * n_samples)` are the minimum number of samples for each node.  .. versionchanged:: 0.18 Added float values for percentages. "
    },
    {
      "default": "0.",
      "type": "float",
      "optional": "true",
      "name": "min_weight_fraction_leaf",
      "description": "The minimum weighted fraction of the sum total of weights (of all the input samples) required to be at a leaf node. Samples have equal weight when sample_weight is not provided. "
    },
    {
      "default": "None",
      "type": "int",
      "optional": "true",
      "name": "max_leaf_nodes",
      "description": "Grow trees with ``max_leaf_nodes`` in best-first fashion. Best nodes are defined as relative reduction in impurity. If None then unlimited number of leaf nodes. "
    },
    {
      "default": "1e-7",
      "type": "float",
      "optional": "true",
      "name": "min_impurity_split",
      "description": "Threshold for early stopping in tree growth. A node will split if its impurity is above the threshold, otherwise it is a leaf.  .. versionadded:: 0.18 "
    },
    {
      "default": "False",
      "type": "boolean",
      "optional": "true",
      "name": "bootstrap",
      "description": "Whether bootstrap samples are used when building trees. "
    },
    {
      "default": "False",
      "type": "bool",
      "optional": "true",
      "name": "oob_score",
      "description": "Whether to use out-of-bag samples to estimate the R^2 on unseen data. "
    },
    {
      "default": "1",
      "type": "integer",
      "optional": "true",
      "name": "n_jobs",
      "description": "The number of jobs to run in parallel for both `fit` and `predict`. If -1, then the number of jobs is set to the number of cores. "
    },
    {
      "default": "None",
      "type": "int",
      "optional": "true",
      "name": "random_state",
      "description": "If int, random_state is the seed used by the random number generator; If RandomState instance, random_state is the random number generator; If None, the random number generator is the RandomState instance used by `np.random`. "
    },
    {
      "default": "0",
      "type": "int",
      "optional": "true",
      "name": "verbose",
      "description": "Controls the verbosity of the tree building process. "
    },
    {
      "default": "False",
      "type": "bool",
      "optional": "true",
      "name": "warm_start",
      "description": "When set to ``True``, reuse the solution of the previous call to fit and add more estimators to the ensemble, otherwise, just fit a whole new forest. "
    }
  ],
  "input_type": [
    "DENSE",
    "SPARSE",
    "UNSIGNED_DATA"
  ],
  "methods_available": [
    {
      "id": "sklearn.ensemble.forest.ExtraTreesRegressor.apply",
      "returns": {
        "shape": "n_samples, n_estimators",
        "type": "array",
        "name": "X_leaves",
        "description": "For each datapoint x in X and for each tree in the forest, return the index of the leaf x ends up in. '"
      },
      "description": "'Apply trees in the forest to X, return leaf indices.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like",
          "name": "X",
          "description": "The input samples. Internally, its dtype will be converted to ``dtype=np.float32``. If a sparse matrix is provided, it will be converted into a sparse ``csr_matrix``. "
        }
      ],
      "name": "apply"
    },
    {
      "id": "sklearn.ensemble.forest.ExtraTreesRegressor.decision_path",
      "returns": {
        "shape": "n_samples, n_nodes",
        "type": "sparse",
        "name": "indicator",
        "description": "Return a node indicator matrix where non zero elements indicates that the samples goes through the nodes.  n_nodes_ptr : array of size (n_estimators + 1, ) The columns from indicator[n_nodes_ptr[i]:n_nodes_ptr[i+1]] gives the indicator value for the i-th estimator.  '"
      },
      "description": "'Return the decision path in the forest\n\n.. versionadded:: 0.18\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like",
          "name": "X",
          "description": "The input samples. Internally, its dtype will be converted to ``dtype=np.float32``. If a sparse matrix is provided, it will be converted into a sparse ``csr_matrix``. "
        }
      ],
      "name": "decision_path"
    },
    {
      "id": "sklearn.ensemble.forest.ExtraTreesRegressor.fit",
      "returns": {
        "type": "object",
        "name": "self",
        "description": "Returns self. '"
      },
      "description": "'Build a forest of trees from the training set (X, y).\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like",
          "name": "X",
          "description": "The training input samples. Internally, its dtype will be converted to ``dtype=np.float32``. If a sparse matrix is provided, it will be converted into a sparse ``csc_matrix``. "
        },
        {
          "shape": "n_samples",
          "type": "array-like",
          "name": "y",
          "description": "The target values (class labels in classification, real numbers in regression). "
        },
        {
          "shape": "n_samples",
          "type": "array-like",
          "name": "sample_weight",
          "description": "Sample weights. If None, then samples are equally weighted. Splits that would create child nodes with net zero or negative weight are ignored while searching for a split in each node. In the case of classification, splits are also ignored if they would result in any single class carrying a negative weight in either child node. "
        }
      ],
      "name": "fit"
    },
    {
      "id": "sklearn.ensemble.forest.ExtraTreesRegressor.fit_transform",
      "returns": {
        "shape": "n_samples, n_features_new",
        "type": "numpy",
        "name": "X_new",
        "description": "Transformed array.  '"
      },
      "description": "'Fit to data, then transform it.\n\nFits transformer to X and y with optional parameters fit_params\nand returns a transformed version of X.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "numpy",
          "name": "X",
          "description": "Training set. "
        },
        {
          "shape": "n_samples",
          "type": "numpy",
          "name": "y",
          "description": "Target values. "
        }
      ],
      "name": "fit_transform"
    },
    {
      "id": "sklearn.ensemble.forest.ExtraTreesRegressor.get_params",
      "returns": {
        "type": "mapping",
        "name": "params",
        "description": "Parameter names mapped to their values. '"
      },
      "description": "'Get parameters for this estimator.\n",
      "parameters": [
        {
          "type": "boolean",
          "optional": "true",
          "name": "deep",
          "description": "If True, will return the parameters for this estimator and contained subobjects that are estimators. "
        }
      ],
      "name": "get_params"
    },
    {
      "id": "sklearn.ensemble.forest.ExtraTreesRegressor.predict",
      "returns": {
        "shape": "n_samples",
        "type": "array",
        "name": "y",
        "description": "The predicted values. '"
      },
      "description": "'Predict regression target for X.\n\nThe predicted regression target of an input sample is computed as the\nmean predicted regression targets of the trees in the forest.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like",
          "name": "X",
          "description": "The input samples. Internally, its dtype will be converted to ``dtype=np.float32``. If a sparse matrix is provided, it will be converted into a sparse ``csr_matrix``. "
        }
      ],
      "name": "predict"
    },
    {
      "id": "sklearn.ensemble.forest.ExtraTreesRegressor.score",
      "returns": {
        "type": "float",
        "name": "score",
        "description": "R^2 of self.predict(X) wrt. y. '"
      },
      "description": "'Returns the coefficient of determination R^2 of the prediction.\n\nThe coefficient R^2 is defined as (1 - u/v), where u is the regression\nsum of squares ((y_true - y_pred) ** 2).sum() and v is the residual\nsum of squares ((y_true - y_true.mean()) ** 2).sum().\nBest possible score is 1.0 and it can be negative (because the\nmodel can be arbitrarily worse). A constant model that always\npredicts the expected value of y, disregarding the input features,\nwould get a R^2 score of 0.0.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like",
          "name": "X",
          "description": "Test samples. "
        },
        {
          "shape": "n_samples",
          "type": "array-like",
          "name": "y",
          "description": "True values for X. "
        },
        {
          "type": "array-like",
          "shape": "n_samples",
          "optional": "true",
          "name": "sample_weight",
          "description": "Sample weights. "
        }
      ],
      "name": "score"
    },
    {
      "id": "sklearn.ensemble.forest.ExtraTreesRegressor.set_params",
      "returns": {
        "name": "self",
        "description": "\""
      },
      "description": "\"Set the parameters of this estimator.\n\nThe method works on simple estimators as well as on nested objects\n(such as pipelines). The latter have parameters of the form\n``<component>__<parameter>`` so that it's possible to update each\ncomponent of a nested object.\n",
      "parameters": [],
      "name": "set_params"
    },
    {
      "id": "sklearn.ensemble.forest.ExtraTreesRegressor.transform",
      "returns": {
        "shape": "n_samples, n_selected_features",
        "type": "array",
        "name": "X_r",
        "description": "The input samples with only the selected features. '"
      },
      "description": "'DEPRECATED: Support to use estimators as feature selectors will be removed in version 0.19. Use SelectFromModel instead.\n\nReduce X to its most important features.\n\nUses ``coef_`` or ``feature_importances_`` to determine the most\nimportant features.  For models with a ``coef_`` for each class, the\nabsolute sum over the classes is used.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array",
          "name": "X",
          "description": "The input samples. "
        },
        {
          "default": "None",
          "type": "string",
          "optional": "true",
          "name": "threshold",
          "description": "The threshold value to use for feature selection. Features whose importance is greater or equal are kept while the others are discarded. If \"median\" (resp. \"mean\"), then the threshold value is the median (resp. the mean) of the feature importances. A scaling factor (e.g., \"1.25*mean\") may also be used. If None and if available, the object attribute ``threshold`` is used. Otherwise, \"mean\" is used by default. "
        }
      ],
      "name": "transform"
    }
  ],
  "common_name_unanalyzed": "Extra Trees Regressor",
  "schema_version": 1.0,
  "languages": [
    "python2.7"
  ],
  "version": "0.18.1",
  "build": [
    {
      "type": "pip",
      "package": "scikit-learn"
    }
  ],
  "handles_multiclass": false,
  "description": "'An extra-trees regressor.\n\nThis class implements a meta estimator that fits a number of\nrandomized decision trees (a.k.a. extra-trees) on various sub-samples\nof the dataset and use averaging to improve the predictive accuracy\nand control over-fitting.\n\nRead more in the :ref:`User Guide <forest>`.\n",
  "tags": [
    "ensemble",
    "forest"
  ],
  "learning_type": [
    "supervised"
  ],
  "task_type": [
    "modeling"
  ],
  "output_type": [
    "PREDICTIONS"
  ],
  "source_code": "https://github.com/scikit-learn/scikit-learn/blob/0.18.X/sklearn/ensemble/forest.py#L1337",
  "category_unanalyzed": "ensemble.forest",
  "name": "sklearn.ensemble.forest.ExtraTreesRegressor",
  "handles_multilabel": false,
  "is_deterministic": true,
  "team": "jpl",
  "attributes": [
    {
      "type": "list",
      "name": "estimators_",
      "description": "The collection of fitted sub-estimators. "
    },
    {
      "shape": "n_features",
      "type": "array",
      "name": "feature_importances_",
      "description": "The feature importances (the higher, the more important the feature). "
    },
    {
      "type": "int",
      "name": "n_features_",
      "description": "The number of features. "
    },
    {
      "type": "int",
      "name": "n_outputs_",
      "description": "The number of outputs. "
    },
    {
      "type": "float",
      "name": "oob_score_",
      "description": "Score of the training dataset obtained using an out-of-bag estimate. "
    },
    {
      "shape": "n_samples",
      "type": "array",
      "name": "oob_prediction_",
      "description": "Prediction computed with out-of-bag estimate on the training set. "
    }
  ]
}
