{
  "handles_regression": true,
  "is_class": true,
  "library": "sklearn",
  "compute_resources": {},
  "common_name": "Decision Tree Classifier",
  "id": "338008eb-abf9-3785-8bc6-bb04c1a1e678",
  "handles_classification": false,
  "category": "tree.tree",
  "source_code_unanalyzed": "https://github.com/scikit-learn/scikit-learn/blob/0.18.X/sklearn/tree/tree.py#L508",
  "parameters": [
    {
      "default": "\"gini\"",
      "type": "string",
      "optional": "true",
      "name": "criterion",
      "description": "The function to measure the quality of a split. Supported criteria are \"gini\" for the Gini impurity and \"entropy\" for the information gain. "
    },
    {
      "default": "\"best\"",
      "type": "string",
      "optional": "true",
      "name": "splitter",
      "description": "The strategy used to choose the split at each node. Supported strategies are \"best\" to choose the best split and \"random\" to choose the best random split. "
    },
    {
      "default": "None",
      "type": "int",
      "optional": "true",
      "name": "max_features",
      "description": "The number of features to consider when looking for the best split:  - If int, then consider `max_features` features at each split. - If float, then `max_features` is a percentage and `int(max_features * n_features)` features are considered at each split. - If \"auto\", then `max_features=sqrt(n_features)`. - If \"sqrt\", then `max_features=sqrt(n_features)`. - If \"log2\", then `max_features=log2(n_features)`. - If None, then `max_features=n_features`.  Note: the search for a split does not stop until at least one valid partition of the node samples is found, even if it requires to effectively inspect more than ``max_features`` features. "
    },
    {
      "default": "None",
      "type": "int",
      "optional": "true",
      "name": "max_depth",
      "description": "The maximum depth of the tree. If None, then nodes are expanded until all leaves are pure or until all leaves contain less than min_samples_split samples. "
    },
    {
      "default": "2",
      "type": "int",
      "optional": "true",
      "name": "min_samples_split",
      "description": "The minimum number of samples required to split an internal node:  - If int, then consider `min_samples_split` as the minimum number. - If float, then `min_samples_split` is a percentage and `ceil(min_samples_split * n_samples)` are the minimum number of samples for each split.  .. versionchanged:: 0.18 Added float values for percentages. "
    },
    {
      "default": "1",
      "type": "int",
      "optional": "true",
      "name": "min_samples_leaf",
      "description": "The minimum number of samples required to be at a leaf node:  - If int, then consider `min_samples_leaf` as the minimum number. - If float, then `min_samples_leaf` is a percentage and `ceil(min_samples_leaf * n_samples)` are the minimum number of samples for each node.  .. versionchanged:: 0.18 Added float values for percentages. "
    },
    {
      "default": "0.",
      "type": "float",
      "optional": "true",
      "name": "min_weight_fraction_leaf",
      "description": "The minimum weighted fraction of the sum total of weights (of all the input samples) required to be at a leaf node. Samples have equal weight when sample_weight is not provided. "
    },
    {
      "default": "None",
      "type": "int",
      "optional": "true",
      "name": "max_leaf_nodes",
      "description": "Grow a tree with ``max_leaf_nodes`` in best-first fashion. Best nodes are defined as relative reduction in impurity. If None then unlimited number of leaf nodes. "
    },
    {
      "default": "None",
      "type": "dict",
      "optional": "true",
      "name": "class_weight",
      "description": "Weights associated with classes in the form ``{class_label: weight}``. If not given, all classes are supposed to have weight one. For multi-output problems, a list of dicts can be provided in the same order as the columns of y.  The \"balanced\" mode uses the values of y to automatically adjust weights inversely proportional to class frequencies in the input data as ``n_samples / (n_classes * np.bincount(y))``  For multi-output, the weights of each column of y will be multiplied.  Note that these weights will be multiplied with sample_weight (passed through the fit method) if sample_weight is specified. "
    },
    {
      "default": "None",
      "type": "int",
      "optional": "true",
      "name": "random_state",
      "description": "If int, random_state is the seed used by the random number generator; If RandomState instance, random_state is the random number generator; If None, the random number generator is the RandomState instance used by `np.random`. "
    },
    {
      "default": "1e-7",
      "type": "float",
      "optional": "true",
      "name": "min_impurity_split",
      "description": "Threshold for early stopping in tree growth. A node will split if its impurity is above the threshold, otherwise it is a leaf.  .. versionadded:: 0.18 "
    },
    {
      "default": "False",
      "type": "bool",
      "optional": "true",
      "name": "presort",
      "description": "Whether to presort the data to speed up the finding of best splits in fitting. For the default settings of a decision tree on large datasets, setting this to true may slow down the training process. When using either a smaller dataset or a restricted depth, this may speed up the training. "
    }
  ],
  "input_type": [
    "DENSE",
    "SPARSE",
    "UNSIGNED_DATA"
  ],
  "methods_available": [
    {
      "id": "sklearn.tree.tree.DecisionTreeClassifier.apply",
      "returns": {
        "shape": "n_samples,",
        "type": "array",
        "name": "X_leaves",
        "description": "For each datapoint x in X, return the index of the leaf x ends up in. Leaves are numbered within ``[0; self.tree_.node_count)``, possibly with gaps in the numbering. \""
      },
      "description": "\"\nReturns the index of the leaf that each sample is predicted as.\n\n.. versionadded:: 0.17\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array",
          "name": "X",
          "description": "The input samples. Internally, it will be converted to ``dtype=np.float32`` and if a sparse matrix is provided to a sparse ``csr_matrix``. "
        },
        {
          "type": "boolean",
          "name": "check_input",
          "description": "Allow to bypass several input checking. Don't use this parameter unless you know what you do. "
        }
      ],
      "name": "apply"
    },
    {
      "id": "sklearn.tree.tree.DecisionTreeClassifier.decision_path",
      "returns": {
        "shape": "n_samples, n_nodes",
        "type": "sparse",
        "name": "indicator",
        "description": "Return a node indicator matrix where non zero elements indicates that the samples goes through the nodes.  \""
      },
      "description": "\"Return the decision path in the tree\n\n.. versionadded:: 0.18\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array",
          "name": "X",
          "description": "The input samples. Internally, it will be converted to ``dtype=np.float32`` and if a sparse matrix is provided to a sparse ``csr_matrix``. "
        },
        {
          "type": "boolean",
          "name": "check_input",
          "description": "Allow to bypass several input checking. Don't use this parameter unless you know what you do. "
        }
      ],
      "name": "decision_path"
    },
    {
      "id": "sklearn.tree.tree.DecisionTreeClassifier.fit",
      "returns": {
        "type": "object",
        "name": "self",
        "description": "Returns self. \""
      },
      "description": "\"Build a decision tree classifier from the training set (X, y).\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like",
          "name": "X",
          "description": "The training input samples. Internally, it will be converted to ``dtype=np.float32`` and if a sparse matrix is provided to a sparse ``csc_matrix``. "
        },
        {
          "shape": "n_samples",
          "type": "array-like",
          "name": "y",
          "description": "The target values (class labels) as integers or strings. "
        },
        {
          "shape": "n_samples",
          "type": "array-like",
          "name": "sample_weight",
          "description": "Sample weights. If None, then samples are equally weighted. Splits that would create child nodes with net zero or negative weight are ignored while searching for a split in each node. Splits are also ignored if they would result in any single class carrying a negative weight in either child node. "
        },
        {
          "type": "boolean",
          "name": "check_input",
          "description": "Allow to bypass several input checking. Don't use this parameter unless you know what you do.  X_idx_sorted : array-like, shape = [n_samples, n_features], optional The indexes of the sorted training input samples. If many tree are grown on the same dataset, this allows the ordering to be cached between trees. If None, the data will be sorted here. Don't use this parameter unless you know what to do. "
        }
      ],
      "name": "fit"
    },
    {
      "id": "sklearn.tree.tree.DecisionTreeClassifier.fit_transform",
      "returns": {
        "shape": "n_samples, n_features_new",
        "type": "numpy",
        "name": "X_new",
        "description": "Transformed array.  '"
      },
      "description": "'Fit to data, then transform it.\n\nFits transformer to X and y with optional parameters fit_params\nand returns a transformed version of X.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "numpy",
          "name": "X",
          "description": "Training set. "
        },
        {
          "shape": "n_samples",
          "type": "numpy",
          "name": "y",
          "description": "Target values. "
        }
      ],
      "name": "fit_transform"
    },
    {
      "id": "sklearn.tree.tree.DecisionTreeClassifier.get_params",
      "returns": {
        "type": "mapping",
        "name": "params",
        "description": "Parameter names mapped to their values. '"
      },
      "description": "'Get parameters for this estimator.\n",
      "parameters": [
        {
          "type": "boolean",
          "optional": "true",
          "name": "deep",
          "description": "If True, will return the parameters for this estimator and contained subobjects that are estimators. "
        }
      ],
      "name": "get_params"
    },
    {
      "id": "sklearn.tree.tree.DecisionTreeClassifier.predict",
      "returns": {
        "shape": "n_samples",
        "type": "array",
        "name": "y",
        "description": "The predicted classes, or the predict values. \""
      },
      "description": "\"Predict class or regression value for X.\n\nFor a classification model, the predicted class for each sample in X is\nreturned. For a regression model, the predicted value based on X is\nreturned.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like",
          "name": "X",
          "description": "The input samples. Internally, it will be converted to ``dtype=np.float32`` and if a sparse matrix is provided to a sparse ``csr_matrix``. "
        },
        {
          "type": "boolean",
          "name": "check_input",
          "description": "Allow to bypass several input checking. Don't use this parameter unless you know what you do. "
        }
      ],
      "name": "predict"
    },
    {
      "id": "sklearn.tree.tree.DecisionTreeClassifier.predict_log_proba",
      "returns": {
        "shape": "n_samples, n_classes",
        "type": "array",
        "name": "p",
        "description": "such arrays if n_outputs > 1. The class log-probabilities of the input samples. The order of the classes corresponds to that in the attribute `classes_`. '"
      },
      "description": "'Predict class log-probabilities of the input samples X.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like",
          "name": "X",
          "description": "The input samples. Internally, it will be converted to ``dtype=np.float32`` and if a sparse matrix is provided to a sparse ``csr_matrix``. "
        }
      ],
      "name": "predict_log_proba"
    },
    {
      "id": "sklearn.tree.tree.DecisionTreeClassifier.predict_proba",
      "returns": {
        "shape": "n_samples, n_classes",
        "type": "array",
        "name": "p",
        "description": "such arrays if n_outputs > 1. The class probabilities of the input samples. The order of the classes corresponds to that in the attribute `classes_`. \""
      },
      "description": "\"Predict class probabilities of the input samples X.\n\nThe predicted class probability is the fraction of samples of the same\nclass in a leaf.\n\ncheck_input : boolean, (default=True)\nAllow to bypass several input checking.\nDon't use this parameter unless you know what you do.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like",
          "name": "X",
          "description": "The input samples. Internally, it will be converted to ``dtype=np.float32`` and if a sparse matrix is provided to a sparse ``csr_matrix``. "
        }
      ],
      "name": "predict_proba"
    },
    {
      "id": "sklearn.tree.tree.DecisionTreeClassifier.score",
      "returns": {
        "type": "float",
        "name": "score",
        "description": "Mean accuracy of self.predict(X) wrt. y.  '"
      },
      "description": "'Returns the mean accuracy on the given test data and labels.\n\nIn multi-label classification, this is the subset accuracy\nwhich is a harsh metric since you require for each sample that\neach label set be correctly predicted.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array-like",
          "name": "X",
          "description": "Test samples. "
        },
        {
          "shape": "n_samples",
          "type": "array-like",
          "name": "y",
          "description": "True labels for X. "
        },
        {
          "type": "array-like",
          "shape": "n_samples",
          "optional": "true",
          "name": "sample_weight",
          "description": "Sample weights. "
        }
      ],
      "name": "score"
    },
    {
      "id": "sklearn.tree.tree.DecisionTreeClassifier.set_params",
      "returns": {
        "name": "self",
        "description": "\""
      },
      "description": "\"Set the parameters of this estimator.\n\nThe method works on simple estimators as well as on nested objects\n(such as pipelines). The latter have parameters of the form\n``<component>__<parameter>`` so that it's possible to update each\ncomponent of a nested object.\n",
      "parameters": [],
      "name": "set_params"
    },
    {
      "id": "sklearn.tree.tree.DecisionTreeClassifier.transform",
      "returns": {
        "shape": "n_samples, n_selected_features",
        "type": "array",
        "name": "X_r",
        "description": "The input samples with only the selected features. '"
      },
      "description": "'DEPRECATED: Support to use estimators as feature selectors will be removed in version 0.19. Use SelectFromModel instead.\n\nReduce X to its most important features.\n\nUses ``coef_`` or ``feature_importances_`` to determine the most\nimportant features.  For models with a ``coef_`` for each class, the\nabsolute sum over the classes is used.\n",
      "parameters": [
        {
          "shape": "n_samples, n_features",
          "type": "array",
          "name": "X",
          "description": "The input samples. "
        },
        {
          "default": "None",
          "type": "string",
          "optional": "true",
          "name": "threshold",
          "description": "The threshold value to use for feature selection. Features whose importance is greater or equal are kept while the others are discarded. If \"median\" (resp. \"mean\"), then the threshold value is the median (resp. the mean) of the feature importances. A scaling factor (e.g., \"1.25*mean\") may also be used. If None and if available, the object attribute ``threshold`` is used. Otherwise, \"mean\" is used by default. "
        }
      ],
      "name": "transform"
    }
  ],
  "common_name_unanalyzed": "Decision Tree Classifier",
  "schema_version": 1.0,
  "languages": [
    "python2.7"
  ],
  "version": "0.18.1",
  "build": [
    {
      "type": "pip",
      "package": "scikit-learn"
    }
  ],
  "handles_multiclass": false,
  "description": "'A decision tree classifier.\n\nRead more in the :ref:`User Guide <tree>`.\n",
  "tags": [
    "tree",
    "classification"
  ],
  "algorithm_type": [
    "classification"
  ],
  "learning_type": [
    "supervised"
  ],
  "task_type": [
    "modeling"
  ],
  "output_type": [
    "PREDICTIONS"
  ],
  "source_code": "https://github.com/scikit-learn/scikit-learn/blob/0.18.X/sklearn/tree/tree.py#L508",
  "category_unanalyzed": "tree.tree",
  "name": "sklearn.tree.tree.DecisionTreeClassifier",
  "handles_multilabel": false,
  "is_deterministic": false,
  "team": "jpl",
  "attributes": [
    {
      "shape": "n_classes",
      "type": "array",
      "name": "classes_",
      "description": "The classes labels (single output problem), or a list of arrays of class labels (multi-output problem). "
    },
    {
      "shape": "n_features",
      "type": "array",
      "name": "feature_importances_",
      "description": "The feature importances. The higher, the more important the feature. The importance of a feature is computed as the (normalized) total reduction of the criterion brought by that feature.  It is also known as the Gini importance [4]_. "
    },
    {
      "type": "int",
      "name": "max_features_",
      "description": "The inferred value of max_features. "
    },
    {
      "type": "int",
      "name": "n_classes_",
      "description": "The number of classes (for single output problems), or a list containing the number of classes for each output (for multi-output problems). "
    },
    {
      "type": "int",
      "name": "n_features_",
      "description": "The number of features when ``fit`` is performed. "
    },
    {
      "type": "int",
      "name": "n_outputs_",
      "description": "The number of outputs when ``fit`` is performed. "
    },
    {
      "type": "",
      "name": "tree_",
      "description": "The underlying Tree object.  See also -------- DecisionTreeRegressor  References ----------  .. [1] https://en.wikipedia.org/wiki/Decision_tree_learning  .. [2] L. Breiman, J. Friedman, R. Olshen, and C. Stone, \"Classification and Regression Trees\", Wadsworth, Belmont, CA, 1984.  .. [3] T. Hastie, R. Tibshirani and J. Friedman. \"Elements of Statistical Learning\", Springer, 2009.  .. [4] L. Breiman, and A. Cutler, \"Random Forests\", http://www.stat.berkeley.edu/~breiman/RandomForests/cc_home.htm "
    }
  ]
}
