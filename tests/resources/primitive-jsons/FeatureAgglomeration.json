{
  "is_class": true, 
  "library": "sklearn", 
  "compute_resources": {}, 
  "common_name": "Feature Agglomeration", 
  "id": "c4d00769-bca1-3233-b562-0ec742f56b37", 
  "category": "cluster.hierarchical", 
  "source_code_unanalyzed": "https://github.com/scikit-learn/scikit-learn/blob/0.18.X/sklearn/cluster/hierarchical.py#L748", 
  "parameters": [
    {
      "type": "int", 
      "name": "n_clusters", 
      "description": "The number of clusters to find. "
    }, 
    {
      "type": "array-like", 
      "optional": "true", 
      "name": "connectivity", 
      "description": "Connectivity matrix. Defines for each feature the neighboring features following a given structure of the data. This can be a connectivity matrix itself or a callable that transforms the data into a connectivity matrix, such as derived from kneighbors_graph. Default is None, i.e, the hierarchical clustering algorithm is unstructured. "
    }, 
    {
      "type": "string", 
      "name": "affinity", 
      "description": "Metric used to compute the linkage. Can be \"euclidean\", \"l1\", \"l2\", \"manhattan\", \"cosine\", or \\'precomputed\\'. If linkage is \"ward\", only \"euclidean\" is accepted. "
    }, 
    {
      "type": "", 
      "optional": "true", 
      "name": "memory", 
      "description": "Used to cache the output of the computation of the tree. By default, no caching is done. If a string is given, it is the path to the caching directory. "
    }, 
    {
      "type": "bool", 
      "optional": "true", 
      "name": "compute_full_tree", 
      "description": "Stop early the construction of the tree at n_clusters. This is useful to decrease computation time if the number of clusters is not small compared to the number of features. This option is useful only when specifying a connectivity matrix. Note also that when varying the number of clusters and using caching, it may be advantageous to compute the full tree. "
    }, 
    {
      "type": "\"ward\", \"complete\", \"average\"", 
      "optional": "true", 
      "name": "linkage", 
      "description": "Which linkage criterion to use. The linkage criterion determines which distance to use between sets of features. The algorithm will merge the pairs of cluster that minimize this criterion.  - ward minimizes the variance of the clusters being merged. - average uses the average of the distances of each feature of the two sets. - complete or maximum linkage uses the maximum distances between all features of the two sets. "
    }, 
    {
      "type": "callable", 
      "name": "pooling_func", 
      "description": "This combines the values of agglomerated features into a single value, and should accept an array of shape [M, N] and the keyword argument `axis=1`, and reduce it to an array of size [M]. "
    }
  ], 
  "tags": [
    "cluster", 
    "hierarchical"
  ], 
  "common_name_unanalyzed": "Feature Agglomeration", 
  "schema_version": 1.0, 
  "languages": [
    "python2.7"
  ], 
  "version": "0.18.1", 
  "build": [
    {
      "type": "pip", 
      "package": "scikit-learn"
    }
  ], 
  "description": "'Agglomerate features.\n\nSimilar to AgglomerativeClustering, but recursively merges features\ninstead of samples.\n\nRead more in the :ref:`User Guide <hierarchical_clustering>`.\n", 
  "methods_available": [
    {
      "id": "sklearn.cluster.hierarchical.FeatureAgglomeration.fit", 
      "returns": {
        "name": "self", 
        "description": "'"
      }, 
      "description": "'Fit the hierarchical clustering on the data\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "array-like", 
          "name": "X", 
          "description": "The data "
        }
      ], 
      "name": "fit"
    }, 
    {
      "id": "sklearn.cluster.hierarchical.FeatureAgglomeration.fit_transform", 
      "returns": {
        "shape": "n_samples, n_features_new", 
        "type": "numpy", 
        "name": "X_new", 
        "description": "Transformed array.  '"
      }, 
      "description": "'Fit to data, then transform it.\n\nFits transformer to X and y with optional parameters fit_params\nand returns a transformed version of X.\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "numpy", 
          "name": "X", 
          "description": "Training set. "
        }, 
        {
          "shape": "n_samples", 
          "type": "numpy", 
          "name": "y", 
          "description": "Target values. "
        }
      ], 
      "name": "fit_transform"
    }, 
    {
      "id": "sklearn.cluster.hierarchical.FeatureAgglomeration.get_params", 
      "returns": {
        "type": "mapping", 
        "name": "params", 
        "description": "Parameter names mapped to their values. '"
      }, 
      "description": "'Get parameters for this estimator.\n", 
      "parameters": [
        {
          "type": "boolean", 
          "optional": "true", 
          "name": "deep", 
          "description": "If True, will return the parameters for this estimator and contained subobjects that are estimators. "
        }
      ], 
      "name": "get_params"
    }, 
    {
      "id": "sklearn.cluster.hierarchical.FeatureAgglomeration.inverse_transform", 
      "returns": {
        "shape": "n_samples, n_features", 
        "type": "array", 
        "name": "X", 
        "description": "A vector of size n_samples with the values of Xred assigned to each of the cluster of samples. '"
      }, 
      "description": "'\nInverse the transformation.\nReturn a vector of size nb_features with the values of Xred assigned\nto each group of features\n", 
      "parameters": [
        {
          "shape": "n_samples, n_clusters", 
          "type": "array-like", 
          "name": "Xred", 
          "description": "The values to be assigned to each cluster of samples "
        }
      ], 
      "name": "inverse_transform"
    }, 
    {
      "id": "sklearn.cluster.hierarchical.FeatureAgglomeration.mean", 
      "returns": {
        "type": "ndarray", 
        "name": "m", 
        "description": "If `out=None`, returns a new array containing the mean values, otherwise a reference to the output array is returned.  See Also -------- average : Weighted average std, var, nanmean, nanstd, nanvar  Notes ----- The arithmetic mean is the sum of the elements along the axis divided by the number of elements.  Note that for floating-point input, the mean is computed using the same precision the input has.  Depending on the input data, this can cause the results to be inaccurate, especially for `float32` (see example below).  Specifying a higher-precision accumulator using the `dtype` keyword can alleviate this issue.  Examples -------- >>> a = np.array([[1, 2], [3, 4]]) >>> np.mean(a) 2.5 >>> np.mean(a, axis=0) array([ 2.,  3.]) >>> np.mean(a, axis=1) array([ 1.5,  3.5])  In single precision, `mean` can be inaccurate:  >>> a = np.zeros((2, 512*512), dtype=np.float32) >>> a[0, :] = 1.0 >>> a[1, :] = 0.1 >>> np.mean(a) 0.546875  Computing the mean in float64 is more accurate:  >>> np.mean(a, dtype=np.float64) 0.55000000074505806  '"
      }, 
      "description": "'\nCompute the arithmetic mean along the specified axis.\n\nReturns the average of the array elements.  The average is taken over\nthe flattened array by default, otherwise over the specified axis.\n`float64` intermediate and return values are used for integer inputs.\n", 
      "parameters": [
        {
          "type": "array", 
          "name": "a", 
          "description": "Array containing numbers whose mean is desired. If `a` is not an array, a conversion is attempted."
        }, 
        {
          "type": "", 
          "optional": "true", 
          "name": "axis", 
          "description": "Axis or axes along which the means are computed. The default is to compute the mean of the flattened array.  .. versionadded: 1.7.0  If this is a tuple of ints, a mean is performed over multiple axes, instead of a single axis or all the axes as before."
        }, 
        {
          "type": "data-type", 
          "optional": "true", 
          "name": "dtype", 
          "description": "Type to use in computing the mean.  For integer inputs, the default is `float64`; for floating point inputs, it is the same as the input dtype."
        }, 
        {
          "type": "ndarray", 
          "optional": "true", 
          "name": "out", 
          "description": "Alternate output array in which to place the result.  The default is ``None``; if provided, it must have the same shape as the expected output, but the type will be cast if necessary. See `doc.ufuncs` for details."
        }, 
        {
          "type": "bool", 
          "optional": "true", 
          "name": "keepdims", 
          "description": "If this is set to True, the axes which are reduced are left in the result as dimensions with size one. With this option, the result will broadcast correctly against the original `arr`. "
        }
      ], 
      "name": "mean"
    }, 
    {
      "id": "sklearn.cluster.hierarchical.FeatureAgglomeration.set_params", 
      "returns": {
        "name": "self", 
        "description": "\""
      }, 
      "description": "\"Set the parameters of this estimator.\n\nThe method works on simple estimators as well as on nested objects\n(such as pipelines). The latter have parameters of the form\n``<component>__<parameter>`` so that it's possible to update each\ncomponent of a nested object.\n", 
      "parameters": [], 
      "name": "set_params"
    }, 
    {
      "id": "sklearn.cluster.hierarchical.FeatureAgglomeration.transform", 
      "returns": {
        "shape": "n_samples, n_clusters", 
        "type": "array", 
        "name": "Y", 
        "description": "The pooled values for each feature cluster. '"
      }, 
      "description": "'\nTransform a new matrix using the built clustering\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "array-like", 
          "name": "X", 
          "description": "A M by N array of M observations in N dimensions or a length M array of M one-dimensional observations. "
        }
      ], 
      "name": "transform"
    }
  ], 
  "algorithm_type": [
    "cluster"
  ], 
  "learning_type": [
    "unsupervised"
  ], 
  "task_type": [
    "modeling", 
    "data preprocessing"
  ], 
  "source_code": "https://github.com/scikit-learn/scikit-learn/blob/0.18.X/sklearn/cluster/hierarchical.py#L748", 
  "category_unanalyzed": "cluster.hierarchical", 
  "name": "sklearn.cluster.hierarchical.FeatureAgglomeration", 
  "team": "jpl", 
  "attributes": [
    {
      "type": "array-like", 
      "name": "labels_", 
      "description": "cluster labels for each feature. "
    }, 
    {
      "type": "int", 
      "name": "n_leaves_", 
      "description": "Number of leaves in the hierarchical tree. "
    }, 
    {
      "type": "int", 
      "name": "n_components_", 
      "description": "The estimated number of connected components in the graph. "
    }, 
    {
      "shape": "n_nodes-1, 2", 
      "type": "array-like", 
      "name": "children_", 
      "description": "The children of each non-leaf node. Values less than `n_features` correspond to leaves of the tree which are the original samples. A node `i` greater than or equal to `n_features` is a non-leaf node and has children `children_[i - n_features]`. Alternatively at the i-th iteration, children[i][0] and children[i][1] are merged to form node `n_features + i`"
    }
  ]
}