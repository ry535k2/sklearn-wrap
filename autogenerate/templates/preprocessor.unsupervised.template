{% extends "base.template" %}
{% block interface %}from d3m.primitive_interfaces.unsupervised_learning import UnsupervisedLearnerPrimitiveBase{% endblock %}


{% block classname %}class SK{{ class_name }}(UnsupervisedLearnerPrimitiveBase[Inputs, Outputs, Params, Hyperparams]):{% endblock %}
    {% block class_variables %}
        self._training_inputs = None
        self._target_names = None
        self._training_indices = None
        self._fitted = False{% endblock %}

    {% block set_training_data %}
    def set_training_data(self, *, inputs: Inputs) -> None:
        self._training_inputs, self._training_indices = self._get_columns_to_fit(inputs, self.hyperparams)
        self._fitted = False
        {% endblock %}

    {% block fit %}
    def fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:
        if self._fitted:
            return CallResult(None)

        if self._training_inputs is None:
            raise ValueError("Missing training data.")

        self._clf.fit(self._training_inputs)
        self._fitted = True

        return CallResult(None)
        {% endblock %}
    {% block produce %}
    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:
        sk_inputs = inputs
        if self.hyperparams['use_semantic_types']:
            sk_inputs = inputs.iloc[:, self._training_indices]
        sk_output = self._clf.transform(sk_inputs)
        if sparse.issparse(sk_output):
            sk_output = sk_output.toarray()
        output = d3m_dataframe(sk_output, generate_metadata=False, source=self)
        {% if flags and "generate_new_metadata" in flags %}output.metadata = inputs.metadata.clear(for_value=output, generate_metadata=True, source=self)
        output.metadata = output.metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, metadata_base.ALL_ELEMENTS), 'https://metadata.datadrivendiscovery.org/types/Attribute', source=self)
        {% else %}output.metadata = common_utils.select_columns_metadata(inputs.metadata, columns=self._training_indices, source=self){% endif %}
        if not self.hyperparams['use_semantic_types']:
            return CallResult(output)
        outputs = common_utils.combine_columns(return_result=self.hyperparams['return_result'],
                                               add_index_columns=self.hyperparams['add_index_columns'],
                                               inputs=inputs, column_indices=self._training_indices, columns_list=[output], source=self)

        return CallResult(outputs)
        {% endblock %}
{% block get_set_params %}{% if params|length == 0 %}
    def get_params(self) -> Params:
        """
        A noop.
        """
        return None

    def set_params(self, *, params: Params) -> None:
        """
        A noop.
        """
        self._fitted = True
        return
{% else %}
    def get_params(self) -> Params:
        if not self._fitted:
            return Params({% for param in params.keys() %}
                {{ param }}=None,{% endfor %}
                training_indices_=self._training_indices,
                target_names_=self._target_names
            )

        return Params({% for param in params.keys() %}
            {{ param }}=getattr(self._clf, '{{ param }}', None),{% endfor %}
            training_indices_=self._training_indices,
            target_names_=self._target_names
        )

    def set_params(self, *, params: Params) -> None:{% for param in params.keys() %}
        self._clf.{{ param }} = params['{{ param }}']{% endfor %}
        self._training_indices = params['training_indices_']
        self._target_names = params['target_names_']
        self._fitted = False
        {% for param in params.keys() %}
        if params['{{ param }}'] is not None:
            self._fitted = True{% endfor %}{% if params|length ==0 %}pass{% endif %}
{% endif %}{% endblock %}
