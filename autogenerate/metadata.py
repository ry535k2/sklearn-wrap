
HYPERPARAM_SEMANTICTYPE_MAPPING = {
    "n_jobs": "https://metadata.datadrivendiscovery.org/types/ResourcesUseParameter",
    "axis": "https://metadata.datadrivendiscovery.org/types/ControlParameter",
    "missing_values": "https://metadata.datadrivendiscovery.org/types/ControlParameter",
}

TUNING_HYPERPARAM = "https://metadata.datadrivendiscovery.org/types/TuningParameter"
