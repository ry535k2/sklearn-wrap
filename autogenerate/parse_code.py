import inspect
import re, json
import sklearn
import hashlib
from sklearn import *
import pkgutil
import pprint


def processReturnLine(content):
    data = {}
    try:
        length = len(content)
        head = 2
        paramlist = []
        temp_data = {}
        end = length
        paramline = str(content[head])
        desc = ' '.join(content[head + 1:end])
        content = content[end + 1:]
        paramline = paramline.split(" : ")
        if len(paramline)>1 :
            temp_data.update(processParamDefLine(str(paramline[1]).strip()))
        temp_data["name"] = paramline[0].strip()
        temp_data["description"] = desc
        data["returns"] = temp_data
    except:
        pass
    return data



def processParamDefLine(content):
    content = content.strip()
    data = {}
    pattern_paranthesis_open = "\(|\{|\["
    pattern_paranthesis_close = "\)|\}|\]"

    # type_pattern = "(^[a-z].*?),*|(^\{.*?\}),*"
    type_pattern = "^([a-z-]*)(\{(.*?)\})?"
    type_match = re.match(type_pattern, content)
    print("type:", content)
    if type_match != None:
        # print type_match.groups(), len(type_match.groups())
        type_str = type_match.group(3) if type_match.group(2)!=None else type_match.group(1)
        data["type"] = str(type_str)

    shape_pattern = ".*shape.*?[\(|\{|\[](.*?)[\)|\}|\]],*.*"
    shape_match = re.match(shape_pattern, content)
    if shape_match != None:
        shape_str = shape_match.group(1)
        data["shape"] = str(shape_str)

    optional_pattern = ".*optional( [\(|\{|\[]default=(.*?)[\)|\}|\]])?,*"
    optional_match = re.match(optional_pattern, content)
    if optional_match != None:
        data["optional"] = "true"
        # print optional_match.groups()
        if optional_match.group(2)  != None:
            data["default"] = str(optional_match.group(2))

    size_pattern = ".*size.*?[\(|\{|\[](.*?)[\)|\}|\]],*.*"
    size_match = re.match(size_pattern, content)
    if size_match != None:
        size_str = size_match.group(1)
        data["size"] = str(size_str)
    return data


def processParam(content, key="parameters"):
    data = {}
    lenght = len(content)
    head = 2
    paramlist = []
    pattern = re.compile("(^[a-zA-Z0-9_()]+[,\sa-z]*) : ")
    while len(content)>1:
        try:
            temp_data = {}
            end = head + 1
            while end < len(content):
                if(re.match(pattern,content[end])):
                    break
                end += 1
            paramline = str(content[head])
            desc = ' '.join(content[head+1:end])
            content = content[end:]
            head = 0
            paramline = paramline.split(":")
            temp_data.update(processParamDefLine(str(paramline[1]).strip()))
            temp_data["name"] = paramline[0].strip()
            temp_data["description"] = desc
            paramlist.append(temp_data)
        except Exception as e:
            # print ("Exception in processing Param")
            print(e)
    data[key] = paramlist
    return data


def describe_func(obj, method=False, prefix=''):
    """ Describe the function object passed as argument.
    If this is a method object, the second argument will
    be passed as True """

    data = dict()

    name = obj.__name__
    qualified_name = ''
    if name[0] == '_':
        return
    if method:
        qualified_name = '%s.%s' % (prefix, name)
    else:
        qualified_name = '%s.%s' % (obj.__module__, obj.__name__)
        name = qualified_name
        data["common_name"] = qualified_name
        data["tags"] = qualified_name.split(".")[1:-1]

    data["name"] = name
    data["id"] = qualified_name
    data.update(processDoc(repr(obj.__doc__).split("\n"), False))

    try:
        arginfo = inspect.getargspec(obj)
    except TypeError:
        print
        return

    args = arginfo[0]
    argsvar = arginfo[1]
    #
    # if args:
    #     if args[0] == 'self':
    #         wi('\t%s is an instance method' % obj.__name__)
    #         args.pop(0)
    #
    #     wi('\t-Method Arguments:', args)
    #
    #     if arginfo[3]:
    #         dl = len(arginfo[3])
    #         al = len(args)
    #         defargs = args[al-dl:al]
    #         wi('\t--Default arguments:',zip(defargs, arginfo[3]))
    #
    # if arginfo[1]:
    #     wi('\t-Positional Args Param: %s' % arginfo[1])
    # if arginfo[2]:
    #     wi('\t-Keyword Args Param: %s' % arginfo[2])
    #
    # print
    return data


def processDoc(obj, isClass=False):
    data = {}
    doc_list = []
    for line in obj:
        # print str(line)
        line = str(line).split("\\n")
        for l in line:
            doc_list.append(l.strip())
    # print doc_list
    param_index = doc_list.index("Parameters") if "Parameters" in doc_list else -1
    return_index = doc_list.index("Returns") if "Returns" in doc_list else -1
    attr_index = doc_list.index("Attributes") if "Attributes" in doc_list else -1
    examples_index = doc_list.index("Examples") if "Examples" in doc_list else -1
    notes_index = doc_list.index("Notes") if "Notes" in doc_list else -1
    ref_index = doc_list.index("References") if "References" in doc_list else -1

    param_end_index = 0
    if(isClass):
        if(attr_index != -1):
            param_end_index = attr_index
        elif (examples_index != -1):
            param_end_index = examples_index
        elif (notes_index != -1):
            param_end_index = notes_index
        elif (ref_index != -1):
            param_end_index = ref_index
        else:
            param_end_index = len(doc_list)
    else:
        param_end_index = return_index if return_index!=-1 else len(doc_list)
    param_list = doc_list[param_index:param_end_index]
    data.update(processParam(param_list))
    if isClass:
        end_index = len(doc_list)-1
        if examples_index != -1:
            end_index = examples_index
        elif notes_index != -1:
            end_index = notes_index
        elif ref_index != -1:
            end_index = ref_index
        attr_list = doc_list[attr_index:end_index]
        data.update(processParam(attr_list, "attributes"))

    desc_index = 0
    desc_end_index = param_index if param_index != -1 else len(doc_list) if return_index==-1 else return_index
    data["description"] = '\n'.join(doc_list[desc_index:desc_end_index])

    if return_index !=-1:
        return_line = doc_list[return_index:]
        data.update(processReturnLine(return_line))

    return data


def describe_klass(obj):
    """ Describe the class object passed as argument,
    including its methods """


    class_name = obj.__name__
    qualified_name = '%s.%s' % (obj.__module__, obj.__name__)
    if 'sklearn' not in qualified_name:
        return

    count = 0
    # if "sklearn.svm.classes.SVC" in qualified_name:
    #     pass
    data = dict()
    data["name"] = qualified_name
    data["id"] = hashlib.md5(str.encode(qualified_name)).hexdigest()
    data["common_name"] = class_name
    data["is_class"] = True
    data["tags"] = qualified_name.split(".")[1:-1]

    try:
        version = str(sklearn.__version__)
        data['version'] = version
        sklearn_git_base = "https://github.com/scikit-learn/scikit-learn/blob/{}".format(version)
        source_code_file = '%s' % (inspect.getfile(obj))
        source_code_file = source_code_file[source_code_file.index('sklearn/'):]
        source_code_line = inspect.getsourcelines(obj)[1]
        
        data["source_code"] = "{}/{}#L{}".format(sklearn_git_base, source_code_file, source_code_line)
    except:
        # print "Could not get source code for %s" % qualified_name
        pass
    data.update(processDoc(repr(obj.__doc__).split("\n"), True))

    methods_list = list()

    object_dict = obj.__dict__
    members = inspect.getmembers(obj)
    for m in members:
        try:
            name, value = m[0], m[1]
            if str(name).startswith('_'):
                continue
            # item = getattr(obj, name)
            if inspect.isfunction(value):
                count += 1
                method_data = describe_func(value, True, prefix=qualified_name)
                if method_data != None:
                    methods_list.append(method_data)

        except Exception as e:
            pass
            # print(str(e))

    data["methods_available"] = methods_list
    return data



def parse_function(name, func_obj):
    metadata = {'name': name}
    metadata.update({'parameters': get_parameters(func_obj)})
    return metadata


def get_parameters(obj):
    parameters = []
    if callable(obj):
        sig = inspect.signature(obj)
        if len(sig.parameters.keys()) > 0:
            for p in sig.parameters.values():
                metadata = {'name': p.name,
                            'default': p.default,
                            'type': type(p.default).__name__}
                parameters.append(metadata)
    return parameters


def parse_class(class_obj):
    metadata = {}
    metadata.update({'parameters': get_parameters(class_obj)})
    methods = []
    for name, obj in inspect.getmembers(class_obj):
        if str(name).startswith('_'):
            continue
        print(name)
        if inspect.isfunction(obj):
            methods.append(parse_function(name, obj))
        else:
            print('not')


def generate_primitive_annotation(class_name: str):
    class_obj = eval(class_name)
    print(sklearn.__version__)
    if inspect.isclass(class_obj):
        # parse_class(class_obj)
        primitive = describe_klass(class_obj)
        print(json.dumps(primitive, indent=4))
        return primitive



